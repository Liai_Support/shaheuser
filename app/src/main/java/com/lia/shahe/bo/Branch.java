package com.lia.shahe.bo;

public class Branch {

    private int id;
    private String bImage;
    private String address;

    public Branch(int id,String bImage,String address) {
        this.id = id;
        this.address = address;
        this.bImage = bImage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getbImage() {
        return bImage;
    }

    public void setbImage(String bImage) {
        this.bImage = bImage;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
