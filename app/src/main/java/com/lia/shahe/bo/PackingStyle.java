package com.lia.shahe.bo;

public class PackingStyle {

    private int pId;
    private String pName;

    public PackingStyle(int pid,String pName){
        this.pId=pid;
        this.pName=pName;
    }

    public int getpId() {
        return pId;
    }

    public void setpId(int pId) {
        this.pId = pId;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }
}

