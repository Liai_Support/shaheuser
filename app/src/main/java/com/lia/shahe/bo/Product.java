package com.lia.shahe.bo;

public class Product {
    private int id;
    private String pname;
    private String pimage;

    public Product(int id,String pname,String pimage) {
        this.id = id;
        this.pname = pname;
        this.pimage = pimage;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getPimage() {
        return pimage;
    }

    public void setPimage(String pimage) {
        this.pimage = pimage;
    }
}