package com.lia.shahe.bo;

public class Banner {
    private int id;
    private String bannerImg;

    public Banner(int id ,String bannerImg) {
        this.id = id;
        this.bannerImg = bannerImg;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBimage() {
        return bannerImg;
    }

    public void setBimage(String bannerImg) {
        this.bannerImg = bannerImg;
    }
}