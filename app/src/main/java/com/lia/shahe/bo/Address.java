package com.lia.shahe.bo;

import org.json.JSONObject;

public class Address {

//    {"id":"2","user_id":"1","address":"1","latitude":"3002","longitude":"3023","buildingname":"","flatno":"","phone":""}
    private int id;
    private int userId;
    private Double lattitude;
    private Double longitude;
    private String addresstxt;
    private String buildingName;
    private String flatNo;
    private String phone;
    private JSONObject Details;



    public Address(int id, int user_id, String address, double latitude, double longitude, String buildingname, String flatno, String phone) {

        this.id =id;
        this.userId =user_id;
        this.lattitude=latitude;
        this.longitude=longitude;
        this.addresstxt=address;
        this.buildingName=buildingname;
        this.flatNo=flatno;
        this.phone=phone;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getLattitude() {
        return lattitude;
    }

    public void setLattitude(Double lattitude) {
        this.lattitude = lattitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getAddresstxt() {
        return addresstxt;
    }

    public void setAddresstxt(String addresstxt) {
        this.addresstxt = addresstxt;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getFlatNo() {
        return flatNo;
    }

    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public JSONObject getDetails() {
        return Details;
    }

    public void setDetails(JSONObject details) {
        Details = details;
    }
}
