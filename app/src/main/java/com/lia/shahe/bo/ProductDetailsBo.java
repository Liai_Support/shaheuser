package com.lia.shahe.bo;

public class    ProductDetailsBo {

    private int ProductDetailsId;
    private  String pDesc,pPrice;
    public ProductDetailsBo(int ProductDetailsId,String pDesc,String pPrice) {
        this.ProductDetailsId = ProductDetailsId;
        this.pDesc = pDesc;
        this.pPrice = pPrice;
    }


    public int getProductDetailsId() {
        return ProductDetailsId;
    }

    public void setProductDetailsId(int productDetailsId) {
        ProductDetailsId = productDetailsId;
    }

    public String getpDesc() {
        return pDesc;
    }

    public void setpDesc(String pDesc) {
        this.pDesc = pDesc;
    }

    public String getpPrice() {
        return pPrice;
    }

    public void setpPrice(String pPrice) {
        this.pPrice = pPrice;
    }
}
