package com.lia.shahe.bo;

public class Order {
    private int id;
    private int apporderId;
    private Double orderTotal;
    private int vat;
    private String paymentType;
    private String orderDate;
    private String orderStatus;
    private String details;
    private String addressDetails;
    public Order(int id,int apporderId,Double orderTotal,int vat,String paymentType,String orderDate,String orderStatus,
                 String details,String addressDetails) {
        this.id = id;
        this.apporderId = apporderId;
        this.orderTotal = orderTotal;
        this.vat = vat;
        this.paymentType = paymentType;
        this.orderDate = orderDate;
        this.orderStatus = orderStatus;
        this.details = details;
        this.addressDetails = addressDetails;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getApporderId() {
        return apporderId;
    }

    public void setApporderId(int apporderId) {
        this.apporderId = apporderId;
    }

    public Double getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(Double orderTotal) {
        this.orderTotal = orderTotal;
    }

    public int getVat() {
        return vat;
    }

    public void setVat(int vat) {
        this.vat = vat;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getAddressDetails() {
        return addressDetails;
    }

    public void setAddressDetails(String addressDetails) {
        this.addressDetails = addressDetails;
    }
}
