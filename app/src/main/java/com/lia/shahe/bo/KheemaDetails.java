package com.lia.shahe.bo;

public class KheemaDetails {
    private int kId;
    private String kDesc,kPrice;

    public KheemaDetails(int kId, String kDesc, String kPrice){
        this.kId=kId;
        this.kDesc=kDesc;
        this.kPrice=kPrice;
    }

    public int getkId() {
        return kId;
    }

    public void setkId(int kId) {
        this.kId = kId;
    }

    public String getkDesc() {
        return kDesc;
    }

    public void setkDesc(String kDesc) {
        this.kDesc = kDesc;
    }

    public String getkPrice() {
        return kPrice;
    }

    public void setpPrice(String kPrice) {
        this.kPrice = kPrice;
    }
}
