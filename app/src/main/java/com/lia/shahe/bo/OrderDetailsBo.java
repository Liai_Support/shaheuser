package com.lia.shahe.bo;

public class OrderDetailsBo {


    private String details;
    public OrderDetailsBo(String details) {

        this.details = details;

    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
