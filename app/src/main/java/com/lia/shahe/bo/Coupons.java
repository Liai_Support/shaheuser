package com.lia.shahe.bo;

public class Coupons {
    private int id;
    private String cCode;
    private String cDesc;
    private Double cAmt;
    private String ctype;

    public Coupons(int id ,String cCode,String cDesc,Double cAmt,String ctype) {
        this.id = id;
        this.cCode = cCode;
        this.cDesc = cDesc;
        this.cAmt = cAmt;
        this.ctype = ctype;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getcCode() {
        return cCode;
    }

    public void setcCode(String cCode) {
        this.cCode = cCode;
    }

    public String getcDesc() {
        return cDesc;
    }

    public void setcDesc(String cDesc) {
        this.cDesc = cDesc;
    }

    public Double getcAmt() {
        return cAmt;
    }

    public void setcAmt(Double cAmt) {
        this.cAmt = cAmt;
    }

    public String getCtype() {
        return ctype;
    }

    public void setCtype(String ctype) {
        this.ctype = ctype;
    }
}
