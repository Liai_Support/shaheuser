package com.lia.shahe.bo;

public class Wallet {
    private int id;
    private String wamount;
    private String wtype;
    private String wdate;
    private String wdesc;

    public Wallet(int id,String wamount,String wtype,String wdate,String wdesc) {
        this.id = id;
        this.wamount = wamount;
        this.wtype = wtype;
        this.wdate = wdate;
        this.wdesc = wdesc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWamount() {
        return wamount;
    }

    public void setWamount(String wamount) {
        this.wamount = wamount;
    }

    public String getWtype() {
        return wtype;
    }

    public void setWtype(String wtype) {
        this.wtype = wtype;
    }

    public String getWdate() {
        return wdate;
    }

    public void setWdate(String wdate) {
        this.wdate = wdate;
    }

    public String getWdesc() {
        return wdesc;
    }

    public void setWdesc(String wdesc) {
        this.wdesc = wdesc;
    }
}
