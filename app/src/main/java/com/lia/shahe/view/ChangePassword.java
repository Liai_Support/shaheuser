package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.shahe.R;
import com.lia.shahe.utility.Session;
import com.lia.shahe.utility.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

public class ChangePassword extends AppCompatActivity implements View.OnClickListener{

    private EditText cpswdtext,pswdtext,conpswdtext;
    private TextView btntext;
    private long lastClickTime = 0;
    private String Url="changepassword";
    private ImageView hfhome,finfo,fcart,flist,fprofile,fwallet;

    private Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        session = new Session(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        toolbar.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);
        cpswdtext=(EditText)findViewById(R.id.cpswdtxt);
        pswdtext=(EditText)findViewById(R.id.pswdtxt);
        conpswdtext=(EditText)findViewById(R.id.conpswdtxt);
        hfhome=(ImageView)findViewById(R.id.fhome);
        finfo=(ImageView)findViewById(R.id.finfo);
        fcart=(ImageView)findViewById(R.id.fcart);
        flist=(ImageView)findViewById(R.id.flistorder);
        fprofile=(ImageView)findViewById(R.id.fprofile);
        fwallet=(ImageView)findViewById(R.id.fwallet);
        btntext=(TextView) findViewById(R.id.btntxt);

        btntext.setOnClickListener(this);
        hfhome.setOnClickListener(this);
        finfo.setOnClickListener(this);
        fcart.setOnClickListener(this);
        flist.setOnClickListener(this);
        fprofile.setOnClickListener(this);
        fwallet.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        // preventing double, using threshold of 1000 ms
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000){
            return;
        }

        lastClickTime = SystemClock.elapsedRealtime();

        if (view==btntext){
            String cpswdStr=cpswdtext.getText().toString();
            String pswdStr=pswdtext.getText().toString();
            String conpswdStr=conpswdtext.getText().toString();
            Log.d("asd1",cpswdStr);
            Log.d("asd2",pswdStr);
            Log.d("asd3",conpswdStr);
            if (pswdStr=="" || cpswdStr=="" || conpswdStr==""){
                Toast.makeText(getApplicationContext(),getString(R.string.enterallfield),Toast.LENGTH_SHORT).show();

            }else if(!pswdStr.equals(conpswdStr)){
                Toast.makeText(getApplicationContext(),getString(R.string.paswordnotmatch),Toast.LENGTH_SHORT).show();
            }else{
                JSONObject requstbody = new JSONObject();
                try {
                    requstbody.put("userid",String.valueOf(session.getUserId()));
                    requstbody.put("password",pswdStr);
                    requstbody.put("oldPassword",cpswdStr);

                    requestJSON(requstbody);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        else if (view == hfhome) {
            Intent intent = new Intent(this, HomeScreen.class);
            startActivity(intent);
        }

        else if (view==fcart){
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);
        }else if (view==finfo){
            Intent i=new Intent(this,AboutUs.class);
            startActivity(i);
        }else if (view==flist){
            Intent i=new Intent(this,OrderListActivity.class);
            startActivity(i);
        }else if (view==fwallet){
            Intent i=new Intent(this,OrderListActivity.class);
            startActivity(i);
        }
        else if (view==fprofile){
            if (StaticInfo.userId!=0) {
                Intent i = new Intent(this, ProfileActivity.class);
                startActivity(i);
            }else {
                Intent i = new Intent(this, SignInActicity.class);
                startActivity(i);
            }
        }

    }

    private void requestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        final String requestBody = response.toString();
        Log.d("requestjsondfdgf", String.valueOf(response));


        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.MAIN_URL+Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("strrrrsdfr", ">>" + response);
//
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error")==true){
                        String msg=obj.getString("message");
                        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
                        Log.d("signin response", ">>" + "error");
                    }else if (obj.getBoolean("error")==false){
                        String msg=obj.getString("message");
                        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();

                        Intent intent=new Intent(getApplicationContext(),HomeScreen.class);
                        startActivity(intent);


                    }
//                        obj.getInt()

                } catch (JSONException e) {
                    e.printStackTrace();
                }
//

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);


    }

    public void setAppLocale(String localeCode){
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
//        prefData.setCurrentLanguage(localeCode);
        editor.putString("My_Lang","ar");
        editor.apply();
        res.updateConfiguration(conf,dm);
    }


}