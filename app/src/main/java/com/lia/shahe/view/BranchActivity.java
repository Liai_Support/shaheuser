package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.lia.shahe.R;
import com.lia.shahe.adapter.BranchAdapter;
import com.lia.shahe.bo.Branch;
import com.lia.shahe.utility.Session;
import com.lia.shahe.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class BranchActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String Url = "Initialvalues";

    private List<Branch> list_branch;
    private ArrayList<Branch> bdetails;
    private ArrayList<String> bdet = new ArrayList<String>();

    private Session session;

    private BranchAdapter branchadapter;
    private GridLayoutManager brgm;
    private RecyclerView branchrv;
    private Spinner branchspinner;
    Button btn;
    TextView sbmtbtn;
    private FirebaseAnalytics mFirebaseAnalytics;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_branch_activity);
        /*if(StaticInfo.lang == false){
            language();
        }*/

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
//        btn=(Button)findViewById(R.id.nbtn);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        toolbar.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);
        session = new Session(this);
         branchspinner = (Spinner) findViewById(R.id.brspinner);

        sbmtbtn = (TextView) findViewById(R.id.sbmt);
        sbmtbtn.setOnClickListener(this);

        branchrv=(RecyclerView)findViewById(R.id.brecycler_view);
        brgm=new GridLayoutManager(this,1,GridLayoutManager.VERTICAL, false);
        branchrv.setLayoutManager(brgm);

        JSONObject obj=new JSONObject();
            try {
                obj.put("userId", StaticInfo.userId);
                getInitialValues(String.valueOf(obj));
                Log.d("initializeuserId",""+obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            btn.setOnClickListener(this);


        branchspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                final Branch listBranch=list_branch.get(i);
                session.setBranchId(listBranch.getId());
                session.setBranchAddr(listBranch.getAddress());



            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });
    }

    private void language() {
        StaticInfo.lang =true;
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.languagepopup, null);
        final AlertDialog alertD = new AlertDialog.Builder(this).create();
        Button arabic = (Button) promptView.findViewById(R.id.arabic);
        Button english = (Button) promptView.findViewById(R.id.english);
        arabic.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                session.setlang("ar");
                setAppLocale("ar");
                Intent n5=new Intent(BranchActivity.this, BranchActivity.class);
                startActivity(n5);
                alertD.dismiss();
                // btnAdd1 has been clicked

            }
        });
        english.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                session.setlang("en");
                setAppLocale("en");
                Intent n5=new Intent(BranchActivity.this, BranchActivity.class);
                startActivity(n5);
                alertD.dismiss();
                // btnAdd2 has been clicked

            }
        });
        alertD.setView(promptView);
        alertD.setCancelable(true);
        alertD.show();
        alertD.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertD.getWindow().getAttributes());
        layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
        alertD.getWindow().setAttributes(layoutParams);
    }

    @Override
    public void onClick(View view) {
        if (view==btn){
            Intent intent =new Intent(BranchActivity.this,HomeScreen.class);
            startActivity(intent);
        } if (view==sbmtbtn){
            Intent intent =new Intent(BranchActivity.this,HomeScreen.class);
            startActivity(intent);
        }

    }

    private void getInitialValues(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(this);
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(response);
            loadingDialog.startLoadingDialog();

            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loadingDialog.dismissDialog();
               try {

                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error").equals(true)){
                            Toast.makeText(getApplicationContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT);
                        }else{
                            JSONObject dataobj=obj.getJSONObject("data");

                            list_branch=new ArrayList<>();
                            JSONArray branchArray=dataobj.getJSONArray("branches");
                            for (int i = 0; i < branchArray.length(); i++) {
                                JSONObject branchobj = branchArray.getJSONObject(i);
                                Branch branch = new Branch(branchobj.getInt("address_id"),branchobj.getString("image"),branchobj.getString("address"));
                                list_branch.add(branch);

                            }

                            //                            new SpinnerVO(0, "King in the north")
                            for (int i = 0; i < list_branch.size(); i++) {

                                bdet.add(list_branch.get(i).getAddress().toString() );
                            }

                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(BranchActivity.this,R.layout.spinner_layout, bdet);
                            spinnerArrayAdapter.setDropDownViewResource(R.layout.dropdown_view_layout); // The drop down view
                            branchspinner.setAdapter(spinnerArrayAdapter);
                            setupBranchData(list_branch);

                           StaticInfo.cartCount= Integer.parseInt(dataobj.getString("cartcount"));
                            if (dataobj.getString("walletAmt") == null) {
                                StaticInfo.walletAmount = Double.valueOf(dataobj.getString("walletAmt"));
                            }else {
                                StaticInfo.walletAmount = 0.0;
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }


            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupBranchData(List<Branch> list_branch) {
        branchadapter=new BranchAdapter(list_branch,this,session);
        branchrv.setAdapter(branchadapter);
    }

    public void setAppLocale(String localeCode){
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
//        prefData.setCurrentLanguage(localeCode);
        editor.putString("My_Lang","ar");
        editor.apply();
        res.updateConfiguration(conf,dm);
    }




}