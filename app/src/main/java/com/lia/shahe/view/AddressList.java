package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.lia.shahe.R;
import com.lia.shahe.adapter.AddressAdapter;
import com.lia.shahe.bo.Address;
import com.lia.shahe.utility.Session;
import com.lia.shahe.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import static com.lia.shahe.view.CartActivity.cartPrice;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AddressList extends AppCompatActivity implements View.OnClickListener{
    private static final String Url = "listaddress";
    private List<Address> list_address;
    private ArrayList<Address> arraylist;
    private Session session;
    private AddressAdapter addressadapter;
    private GridLayoutManager argm;
    private RecyclerView addressrv;
    private TextView addadresstxt,noprodtxt;
    private long lastClickTime = 0;
    private ImageView hfhome,finfo,fcart,flist,fprofile,fwallet;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);
        hfhome=(ImageView)findViewById(R.id.fhome);
        finfo=(ImageView)findViewById(R.id.finfo);
        fcart=(ImageView)findViewById(R.id.fcart);
        flist=(ImageView)findViewById(R.id.flistorder);
        fprofile=(ImageView)findViewById(R.id.fprofile);
        fwallet=(ImageView)findViewById(R.id.fwallet);
        session = new Session(this);
        this.list_address = list_address;
        addadresstxt=(TextView) findViewById(R.id.addadress);
        noprodtxt=(TextView) findViewById(R.id.noprodtxt);
        addressrv=(RecyclerView)findViewById(R.id.arecycler_view);
        argm=new GridLayoutManager(this,1,GridLayoutManager.VERTICAL, false);
        addressrv.setLayoutManager(argm);


        addadresstxt.setOnClickListener(this);
        hfhome.setOnClickListener(this);
        finfo.setOnClickListener(this);
        fcart.setOnClickListener(this);
        flist.setOnClickListener(this);
        fprofile.setOnClickListener(this);
        fwallet.setOnClickListener(this);

        JSONObject obj=new JSONObject();
        try {
            obj.put("userId", StaticInfo.userId);
            getAddressValues(String.valueOf(obj));
            Log.d("initializeuserId",""+obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void getAddressValues(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(this);
        loadingDialog.startLoadingDialog();

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(response);

            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        loadingDialog.dismissDialog();
                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error")=="true"){
//                            noprodtxt.setVisibility(View.VISIBLE);
                            Toast.makeText(getApplicationContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT);
                        }else{
                            Log.d("asdfghj",""+obj.getJSONArray("data"));
//                            JSONArray dataobj=obj.getJSONArray("data");
//                            {"id":"2","user_id":"1","address":"1","latitude":"3002","longitude":"3023","buildingname":"","flatno":"","phone":""}
                            list_address=new ArrayList<>();
                            JSONArray dataArray=obj.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                Log.d("asdfghj",""+dataArray.getJSONObject(i));
                                JSONObject addresshobj = dataArray.getJSONObject(i);
                                Address address = new Address(
                                        addresshobj.getInt("id"),
                                        addresshobj.getInt("user_id"),
                                        addresshobj.getString("address"),addresshobj.getDouble("latitude"),
                                        addresshobj.getDouble("longitude"),addresshobj.getString("buildingname"),
                                        addresshobj.getString("flatno"),addresshobj.getString("phone")
                                   );
                                list_address.add(address);

                            }
                            setupAddressData(list_address);


                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }


            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupAddressData(List<Address> list_address) {
        addressadapter=new AddressAdapter(list_address,this,session);

        addressrv.setAdapter(addressadapter);
    }



    @Override
    public void onClick(View view) {
        // preventing double, using threshold of 1000 ms
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000){
            return;
        }

        lastClickTime = SystemClock.elapsedRealtime();

        if (view==addadresstxt){
            if (ContextCompat.checkSelfPermission(AddressList.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // Permission is not granted
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(AddressList.this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                    new AlertDialog.Builder(this)
                            .setTitle("Required Location Permission")
                            .setMessage("You have to give this permission to acess this feature")
                            .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    ActivityCompat.requestPermissions(AddressList.this,
                                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                            MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

                                }
                            })
                            .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .create()
                            .show();


                } else {
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(AddressList.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }else{
                Intent intent = new Intent(AddressList.this, AddressActivity.class);
                startActivity(intent);

            }


        


        }

        else if (view == hfhome) {
            Intent intent = new Intent(AddressList.this, HomeScreen.class);
            startActivity(intent);
        }

        else if (view==fcart){
            Intent intent = new Intent(AddressList.this, CartActivity.class);
            startActivity(intent);
        }else if (view==finfo){
            Intent i=new Intent(this,AboutUs.class);
            startActivity(i);
        }else if (view==flist){
            Intent i=new Intent(this,OrderListActivity.class);
            startActivity(i);
        }else if (view==fwallet){
            Intent i=new Intent(this,WalletHistory.class);
            startActivity(i);
        }
        else if (view==fprofile){
            if (StaticInfo.userId!=0) {
                Intent i = new Intent(this, ProfileActivity.class);
                startActivity(i);
            }else {
                Intent i = new Intent(this, SignInActicity.class);
                startActivity(i);
            }
        }
    }




    @Override
    public void onBackPressed() {
        cartPrice=0.0;
        finish();
        Intent intent = new Intent(AddressList.this, HomeScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
    public void setAppLocale(String localeCode){
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
//        prefData.setCurrentLanguage(localeCode);
        editor.putString("My_Lang","ar");
        editor.apply();
        res.updateConfiguration(conf,dm);
    }

}