package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.shahe.R;
import com.lia.shahe.utility.Session;
import com.lia.shahe.utility.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

public class PasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView fpswdtxt,fcpswdtxt,sbmttxt;
    private  String mobileStr;
    private Session session;
    private String Url="resetpassword";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        session=new Session(this);

        mobileStr=getIntent().getStringExtra("phoneNo");

        fpswdtxt=findViewById(R.id.fpswd);
        fcpswdtxt=findViewById(R.id.fcpswd);
        sbmttxt=findViewById(R.id.fsbmt);


        sbmttxt.setOnClickListener(this);



    }

    @Override
    public void onClick(View view) {
        if (view==sbmttxt){
            String cpswdStr=fpswdtxt.getText().toString();
            String pswdStr=fcpswdtxt.getText().toString();

            if (pswdStr=="" ||  cpswdStr==""){
                Toast.makeText(getApplicationContext(),getString(R.string.enterallfield),Toast.LENGTH_SHORT).show();

            }else if(!pswdStr.equals(cpswdStr)){
                Toast.makeText(getApplicationContext(),getString(R.string.paswordnotmatch),Toast.LENGTH_SHORT).show();
            }else{
                JSONObject requstbody = new JSONObject();
                try {
                    requstbody.put("phone",String.valueOf("966"+mobileStr));
                    requstbody.put("password",pswdStr);


                    requestJSON(requstbody);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }


    private void requestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        final String requestBody = response.toString();
        Log.d("requestjsondfdgf", String.valueOf(response));


        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.MAIN_URL_V3+Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("strrrrsdfr", ">>" + response);
//
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error")==true){
                        String msg=obj.getString("message");
                        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
                        Log.d("signin response", ">>" + "error");
                    }else if (obj.getBoolean("error")==false){
                        String msg=obj.getString("message");
                        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();

                        Intent intent=new Intent(getApplicationContext(),SignInActicity.class);
                        startActivity(intent);


                    }
//                        obj.getInt()

                } catch (JSONException e) {
                    e.printStackTrace();
                }
//

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);


    }

    public void setAppLocale(String localeCode){
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
//        prefData.setCurrentLanguage(localeCode);
        editor.putString("My_Lang","ar");
        editor.apply();
        res.updateConfiguration(conf,dm);
    }
}