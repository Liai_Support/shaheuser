package com.lia.shahe.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.navigation.NavigationView;
import com.lia.shahe.R;
import com.lia.shahe.adapter.BannerAdapter;
import com.lia.shahe.adapter.ProductAdapter;
import com.lia.shahe.bo.Banner;
import com.lia.shahe.bo.Product;
import com.lia.shahe.utility.Session;
import com.lia.shahe.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class HomeScreen extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, MenuItem.OnMenuItemClickListener {

    private static final String productUrl = "listProdCategory";
    private static final String bannerUrl = "listBannerImg";
    private static final String Url = "Initialvaluesandroid";

    private List<Product> list_data;
    private List<Banner> list_banner;
    private ProductAdapter padapter;
    private BannerAdapter badapter;
    private RecyclerView prv,brv;
    private GridLayoutManager pgm,bgm;
    private JsonArrayRequest prequest,brequest;
    private RequestQueue prequestQueue,brequestQueue;
    private ImageView hfhome,finfo,fcart,flist,fprofile,fwallet;
    public static int notificationCountCart = 0;
    private TextView btxt,textCartItemCount;
    int mCartItemCount = 0;
    private  MenuItem mitem;
    private Session session;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    boolean doubleBackToExitPressedOnce = false;
    private static Context mContext;
    private NavigationView navigationView;
    private long lastClickTime = 0;
    Timer timer;
    TimerTask timerTask;
    int position;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        Log.d("cartcount",""+StaticInfo.cartCount);
        hfhome=(ImageView)findViewById(R.id.fhome);
        finfo=(ImageView)findViewById(R.id.finfo);
        fcart=(ImageView)findViewById(R.id.fcart);
        flist=(ImageView)findViewById(R.id.flistorder);
        fprofile=(ImageView)findViewById(R.id.fprofile);
        fwallet=(ImageView)findViewById(R.id.fwallet);
        mitem = (MenuItem)findViewById(R.id.cart);
        hfhome.setImageResource(R.drawable.ic_home_selected);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
         btxt = (TextView) findViewById(R.id.brtext);
        mContext = HomeScreen.this;
        session = new Session(this);
        btxt.setText(session.getBranchAddr());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        //Log.d("alert",)
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_list);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        JSONObject obj=new JSONObject();
        try {
            obj.put("userId", session.getUserId());
            obj.put("branchId", session.getBranchId());
            getInitialValues(String.valueOf(obj));
            Log.d("initializeuserId",""+obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        brv=(RecyclerView)findViewById(R.id.recyclerbanner);
        bgm=new GridLayoutManager(this,1,GridLayoutManager.HORIZONTAL, false);
        brv.setLayoutManager(bgm);

//        list_banner=new ArrayList<Banner>();
        int blargePadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing);
        int bsmallPadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing_small);
        brv.addItemDecoration(new ProductGridItemDecoration(blargePadding, bsmallPadding));

        prv=(RecyclerView)findViewById(R.id.recycler_view);
        pgm=new GridLayoutManager(this,3,GridLayoutManager.VERTICAL, false);
        prv.setLayoutManager(pgm);
        list_data=new ArrayList<>();
        final int largePadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing);
        int smallPadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing_small);
        prv.addItemDecoration(new ProductGridItemDecoration(largePadding, smallPadding));

        hfhome.setOnClickListener(this);
        finfo.setOnClickListener(this);
        fcart.setOnClickListener(this);
        flist.setOnClickListener(this);
        fprofile.setOnClickListener(this);
        fwallet.setOnClickListener(this);
        btxt.setOnClickListener(this);
        runAutoScrollBanner();



    }


    private void getInitialValues(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(this);
        try {
            loadingDialog.startLoadingDialog();
            RequestQueue requestQueue = Volley.newRequestQueue(this);
//            String URL = "http://...";
            JSONObject jsonBody = new JSONObject(response);

            final String requestcartBody = jsonBody.toString();
            Log.d("requestjsondfdgf", String.valueOf(StaticInfo.MAIN_URL+Url));
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loadingDialog.dismissDialog();
                    Log.d("strrrrsdfr", ">>" + response);
//
                    try {

                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error").equals(true)){
                            Toast.makeText(getApplicationContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT);
                        }else{
                            JSONObject dataobj=obj.getJSONObject("data");
//                            Log.d("asdfg", String.valueOf(dataobj.getJSONArray("banner")));
                            list_banner = new ArrayList<>();
                            JSONArray bannerArray=dataobj.getJSONArray("banner");
                            Log.d("id", String.valueOf(bannerArray));
                            for (int i = 0; i < bannerArray.length(); i++) {
                                JSONObject bannerobj = bannerArray.getJSONObject(i);
                                Banner banner = new Banner(bannerobj.getInt("id"),bannerobj.getString("image"));
                                list_banner.add(banner);
                                Log.w("sd",""+list_banner.size());
                            }
                            setupBannerData(list_banner);
                            list_data=new ArrayList<>();
                            JSONArray productArray=dataobj.getJSONArray("category");
                            for (int i = 0; i < productArray.length(); i++) {
                                JSONObject productrobj = productArray.getJSONObject(i);
                                Product product = new Product(productrobj.getInt("pId"),productrobj.getString("pName"),productrobj.getString("image"));
                                list_data.add(product);
                                setupProductData(list_data);
                                Log.w("sd",""+list_data.size());
                            }

                            JSONObject aboutobj=dataobj.getJSONObject("aboutUs");
                            session.setacontent(aboutobj.getString("content"));
                            session.setavat(aboutobj.getString("vat"));
                            session.setahelp(aboutobj.getString("help"));
                            session.setacontactUs(aboutobj.getString("contactUs"));
                            StaticInfo.walletAmount= Double.valueOf(dataobj.getString("walletAmt"));


                        }
//                        obj.getInt()



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }


            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    ////////////////banner start/////////////////////

    private void setupBannerData(List<Banner> list_banner) {
        badapter=new BannerAdapter(list_banner,this);
        brv.setAdapter(badapter);

        if (list_banner != null) {
            position = Integer.MAX_VALUE/2 ;
            brv.scrollToPosition(position);
        }
        brv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == 1) {
//                    Log.d("aadd",""+newState);
                    position = bgm.findFirstCompletelyVisibleItemPosition();
                    runAutoScrollBanner();
                } else if (newState == 0) {
//                    Log.d("aadd0",""+newState);
                    position = bgm.findFirstCompletelyVisibleItemPosition();
                    runAutoScrollBanner();
                }
            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        stopAutoScrollBanner();
    }

    private void stopAutoScrollBanner() {
        if (timer != null && timerTask != null) {
            timerTask.cancel();
            timer.cancel();
            timer = null;
            timerTask = null;
            position = bgm.findFirstCompletelyVisibleItemPosition();
        }
    }

    private void runAutoScrollBanner() {
        if (timer == null && timerTask == null) {
            timer = new Timer();
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    if (position == Integer.MAX_VALUE) {
                        position = Integer.MAX_VALUE /Integer.MAX_VALUE;
                        brv.scrollToPosition(position);
                        brv.smoothScrollBy(3, 4);
                    } else {
                        position++;
                        brv.smoothScrollToPosition(position);
                    }
                }
            };
            timer.schedule(timerTask, 4000,4000);
        }
    }



    ////////////////banner end//////////////////////

    ////////////////////////product Start///////////////////////

    private void setupProductData(List<Product> list_data) {
        padapter=new ProductAdapter(list_data,this);
        prv.setAdapter(padapter);

    }
    ////////////////////////product end///////////////////////

    //////////////////////Navigation Action///////////////////////
    @SuppressLint("ResourceAsColor")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
//        Typeface typeface = Typeface.createFromAsset(getApplicationContext().getAssets(), "font/droidkufiregular.ttf");
        if (id == R.id.dashboard) {
            startActivity(new Intent(HomeScreen.this, HomeScreen.class));
        }else  if (id == R.id.changepassword) {
            if (session.getUserId()==0) {
                TextView content = new TextView(this);
                final Typeface typeface = ResourcesCompat.getFont(this, R.font.droidkufiregular);
                content.setMaxLines(3);
                content.setPadding(15,15,15,15);
                content.setTextColor(R.color.Black);
                content.setTextSize(12);

                       content.setText(getString(R.string.please_login));
                content.setTypeface(typeface);
                Context context = new ContextThemeWrapper(this,R.style.CustomFontTheme);
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(getString(R.string.alert));
                builder.setView(content);
                builder.setCancelable(false);
                builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        startActivity(new Intent(HomeScreen.this, SignInActicity.class));
                    }
                });

                builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Toast.makeText(getApplicationContext(), "You've changed your mind to delete all records", Toast.LENGTH_SHORT).show();
                    }
                });

                builder.show();
            }else{

                startActivity(new Intent(HomeScreen.this, ChangePassword.class));
            }
        }else  if (id == R.id.logoutMenu) {

            if (StaticInfo.userId!=0) {
                TextView content = new TextView(this);
                final Typeface typeface = ResourcesCompat.getFont(this, R.font.droidkufiregular);
                content.setMaxLines(3);
                content.setPadding(15,15,15,15);

                content.setText(getString(R.string.are_you_want));
                content.setTypeface(typeface);
                Context context = new ContextThemeWrapper(this,R.style.CustomFontTheme);
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(getString(R.string.confirm));
                builder.setView(content);
                builder.setCancelable(false);
                builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                     session.logoutUser();
                        StaticInfo.userId = 0;
                        StaticInfo.cartCount = 0;
                        session.setUserId(0);
                        startActivity(new Intent(HomeScreen.this, HomeScreen.class));
                    }
                });

                builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Toast.makeText(getApplicationContext(), "You've changed your mind to delete all records", Toast.LENGTH_SHORT).show();
                    }
                });

                builder.show();
            }else{

                startActivity(new Intent(HomeScreen.this, SignInActicity.class));
            }
        }


        else if (id == R.id.my_address){
//            getAddress();

            startActivity(new Intent(HomeScreen.this, AddressList.class));
        }else if (id == R.id.contact_us){
            startActivity(new Intent(HomeScreen.this, AboutUs.class));
        }else if (id == R.id.share){
            final String appPackageName = this.getPackageName();
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "تحقق من هذا التطبيق"+": https://play.google.com/store/apps/details?id=" + appPackageName);
            sendIntent.setType("text/plain");
            this.startActivity(sendIntent);


        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        overridePendingTransition(0,0);

        return true;
    }

    private void getAddress() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION )==PackageManager.PERMISSION_GRANTED){
            startActivity(new Intent(this,AddressList.class));
            }else{
                if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)){
                Toast.makeText(getApplicationContext(),"get camera permission",Toast.LENGTH_SHORT).show();
                }
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }


        }


    }

////////////////////////Navigation Action End /////////////






///////////////////////menu start/////////////////////////
@Override
public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_dashboard, menu);

    final MenuItem menuItem = menu.findItem(R.id.cart);

    View actionView = menuItem.getActionView();
    textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);

    setupBadge();

    actionView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onOptionsItemSelected(menuItem);
        }
    });

    return true;
}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.cart: {
                // Do something
                cartAction();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }


    private void cartAction() {
        Intent i=new Intent(this,CartActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }


    private void setupBadge() {

        if (textCartItemCount != null) {
            if (StaticInfo.cartCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(StaticInfo.cartCount, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }







///////////////////////menu end ////////////////////////
////////////////onclick Start////////////////////////
@Override
public void onClick(View view) {
    // preventing double, using threshold of 1000 ms
    if (SystemClock.elapsedRealtime() - lastClickTime < 1000){
        return;
    }

    lastClickTime = SystemClock.elapsedRealtime();

    if (view == hfhome) {
        Intent intent = new Intent(HomeScreen.this, HomeScreen.class);
        startActivity(intent);
    }
    else if (view == btxt){
        Intent intent = new Intent(HomeScreen.this, BranchActivity.class);
        startActivity(intent);
    }
   else if (view==fcart){
        Intent intent = new Intent(HomeScreen.this, CartActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }else if (view==finfo){
        Intent i=new Intent(this,AboutUs.class);
        startActivity(i);
    }else if (view==flist){
        Intent i=new Intent(this,OrderListActivity.class);
        startActivity(i);
    }else if (view==fwallet){
        Intent i=new Intent(this,WalletHistory.class);
        startActivity(i);
    }
    else if (view==fprofile){
        if (StaticInfo.userId!=0) {
            Intent i = new Intent(this, ProfileActivity.class);
            startActivity(i);
        }else {
            Intent i = new Intent(this, SignInActicity.class);
            startActivity(i);
        }
    }

}

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        if (menuItem==mitem){
            Log.w("asdfvgsdf","asdfg");

        }
        return true;
    }
//////////////onclickEnd///////////////////////

    public void setAppLocale(String localeCode){
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
//        prefData.setCurrentLanguage(localeCode);
        editor.putString("My_Lang","ar");
        editor.apply();
        res.updateConfiguration(conf,dm);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.pleaseclickback), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

}