package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.shahe.R;
import com.lia.shahe.utility.Session;
import com.lia.shahe.utility.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

import static android.widget.Toast.LENGTH_LONG;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView signin,signupBtn;
    private EditText name,mobno,email,pswd,cpswd,referalcodetxt;
    private String URLstring="customersignup";
    private Session session;
    private String phoneNo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        signin=(TextView)findViewById(R.id.signinbtn);
        signupBtn=(TextView)findViewById(R.id.signupbtn);
        name=(EditText) findViewById(R.id.sname);
        mobno=(EditText) findViewById(R.id.mobileno);
        email=(EditText) findViewById(R.id.smail_id);
        pswd=(EditText) findViewById(R.id.password);
        cpswd=(EditText) findViewById(R.id.confirmpass);
        referalcodetxt=(EditText) findViewById(R.id.referal);
        session = new Session(this);

        signupBtn.setOnClickListener(this);
        signin.setOnClickListener(this);

        phoneNo=getIntent().getStringExtra("phoneNo");
        mobno.setEnabled(false);
        mobno.setText(phoneNo);

    }

    @Override
    public void onClick(View view) {

        if (view ==signupBtn){
            Toast.makeText(getApplicationContext(),"dfghjk",LENGTH_LONG);
            Log.d("asd","asdc");
            String nameStr=name.getText().toString();
            String mobStr=mobno.getText().toString();
            String emailStr=email.getText().toString();
            String pswdStr=pswd.getText().toString();
            String cpswdStr=cpswd.getText().toString();
            String refStr=referalcodetxt.getText().toString();
            Log.d("asdf",""+mobStr);
            if (pswdStr==""||nameStr==""||mobStr==""||emailStr==""||cpswdStr=="") {

                Toast.makeText(this,getString(R.string.enterallfield),Toast.LENGTH_SHORT).show();
            }else if(!pswdStr.equals(cpswdStr)){
                Toast.makeText(this,getString(R.string.paswordnotmatch),Toast.LENGTH_SHORT).show();
            }
            else{
                JSONObject requstbody = new JSONObject();
                try {
                    requstbody.put("name", nameStr);
                    requstbody.put("mobile", "966"+mobStr);
                    requstbody.put("email", emailStr);
                    requstbody.put("password", pswdStr);
                    requstbody.put("referBy", refStr);
                    Log.d("request", String.valueOf(requstbody));
                    requestJSON(requstbody);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }





        }
        else if (view==signin){
            Intent intent=new Intent(getApplicationContext(),SignInActicity.class);
            startActivity(intent);

        }

    }

    private void requestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
//            String URL = "http://...";
//            JSONObject jsonBody = new JSONObject(response);

        final String requestBody = response.toString();
        Log.d("requestjsondfdgf", String.valueOf(response));


        StringRequest stringRequest = new StringRequest(Request.Method.POST,StaticInfo.MAIN_URL+URLstring, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("strrrrsdfr", ">>" + response);
//
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error")==true){
                        String msg=obj.getString("message");
                        Log.d("signin response", ">>" + "error");
                        Toast.makeText(getApplicationContext(),getString(R.string.enterallfield),Toast.LENGTH_SHORT).show();
                    }else if (obj.getBoolean("error")==false){
                        String msg=obj.getString("message");
                        Toast.makeText(getApplicationContext(),getString(R.string.userhasbeen),Toast.LENGTH_SHORT).show();
                        JSONObject dataobj = new JSONObject(String.valueOf(obj.getJSONObject("data")));
                        StaticInfo.userId= Integer.parseInt(dataobj.getString("custId"));
                        session.setUserId(Integer.parseInt(dataobj.getString("custId")));
                        Log.d("uId", String.valueOf(StaticInfo.userId));
                        Intent intent=new Intent(getApplicationContext(),HomeScreen.class);
                        startActivity(intent);

                    }
//                        obj.getInt()



                } catch (JSONException e) {
                    e.printStackTrace();
                }
//


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }


        };

        requestQueue.add(stringRequest);
    }
    public void setAppLocale(String localeCode){
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
//        prefData.setCurrentLanguage(localeCode);
        editor.putString("My_Lang","ar");
        editor.apply();
        res.updateConfiguration(conf,dm);
    }
    @Override
    public void onBackPressed() {

        Intent intent = new Intent(SignUpActivity.this, SignupMobile.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

}