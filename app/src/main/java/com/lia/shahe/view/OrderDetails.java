package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.lia.shahe.R;
import com.lia.shahe.adapter.OrderDetailAdapter;
import com.lia.shahe.bo.OrderDetailsBo;
import com.lia.shahe.utility.Session;
import com.lia.shahe.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class OrderDetails extends AppCompatActivity implements View.OnClickListener{
    private String oDetails="";
    private List<OrderDetailsBo> list_orderbo;
    private ArrayList<OrderDetailsBo> arraylist;
    private Session session;
    private OrderDetailAdapter orderdetadapter;
    private GridLayoutManager crgm;
    private RecyclerView cartrv;
    private ImageView hfhome,finfo,fcart,flist,fprofile,fwallet;
    private long lastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        oDetails=getIntent().getStringExtra("details");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);
        session = new Session(this);
        this.list_orderbo = list_orderbo;
        hfhome=(ImageView)findViewById(R.id.fhome);
        finfo=(ImageView)findViewById(R.id.finfo);
        fcart=(ImageView)findViewById(R.id.fcart);
        flist=(ImageView)findViewById(R.id.flistorder);
        fprofile=(ImageView)findViewById(R.id.fprofile);
        fwallet  =(ImageView)findViewById(R.id.fwallet);

        cartrv=(RecyclerView)findViewById(R.id.crecycler_view);
        crgm=new GridLayoutManager(this,1,GridLayoutManager.VERTICAL, false);
        cartrv.setLayoutManager(crgm);
        Log.d("detailsact",oDetails);

        list_orderbo=new ArrayList<>();

        try {
            JSONArray   dataArray = new JSONArray(oDetails);
            Log.d("detailsactasd",""+dataArray);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject orderobj = dataArray.getJSONObject(i);
                OrderDetailsBo orderdet = new OrderDetailsBo(orderobj.getString("details"));
                list_orderbo.add(orderdet);
                setuporderData(list_orderbo);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        hfhome.setOnClickListener(this);
        finfo.setOnClickListener(this);
        fcart.setOnClickListener(this);
        flist.setOnClickListener(this);
        fprofile.setOnClickListener(this);
        fwallet.setOnClickListener(this);

    }
    private void setuporderData(List<OrderDetailsBo> list_orderbo) {
        orderdetadapter=new OrderDetailAdapter(list_orderbo,this,session,this);

        cartrv.setAdapter(orderdetadapter);
    }

    @Override
    public void onClick(View view) {
        // preventing double, using threshold of 1000 ms
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000){
            return;
        }

        lastClickTime = SystemClock.elapsedRealtime();



         if (view == hfhome) {
            Intent intent = new Intent(this, HomeScreen.class);
            startActivity(intent);
        }

        else if (view==fcart){
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);
        }else if (view==finfo){
            Intent i=new Intent(this,AboutUs.class);
            startActivity(i);
        }else if (view==flist){
            Intent i=new Intent(this,OrderListActivity.class);
            startActivity(i);
        }else if (view==fwallet){
            Intent i=new Intent(this,WalletHistory.class);
            startActivity(i);
        }
        else if (view==fprofile){
            if (StaticInfo.userId!=0) {
                Intent i = new Intent(this, ProfileActivity.class);
                startActivity(i);
            }else {
                Intent i = new Intent(this, SignInActicity.class);
                startActivity(i);
            }
        }
    }
    @Override
    public void onBackPressed() {

        finish();
        Intent intent = new Intent(OrderDetails.this, HomeScreen.class);
        startActivity(intent);
    }
    public void setAppLocale(String localeCode){
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
//        prefData.setCurrentLanguage(localeCode);
        editor.putString("My_Lang","ar");
        editor.apply();
        res.updateConfiguration(conf,dm);
    }

}