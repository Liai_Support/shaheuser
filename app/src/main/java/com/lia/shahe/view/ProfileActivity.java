package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.shahe.R;
import com.lia.shahe.utility.Session;
import com.lia.shahe.utility.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

import static android.view.View.VISIBLE;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText nametxt,phonetxt,emailtxt;
    private TextView confirmtxt,referfriendtxt;
    private Session session;
    private String Url="viewprofile";
    private String updateUrl="updateprofile";
    private ImageView hfhome,finfo,fcart,flist,fprofile,fwallet;
    private long lastClickTime = 0;
    private String referCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);
        session = new Session(this);
        nametxt=(EditText)findViewById(R.id.pname);
        phonetxt=(EditText)findViewById(R.id.pmobileno);
        emailtxt=(EditText)findViewById(R.id.pmailid);
        confirmtxt=(TextView)findViewById(R.id.pconfirm);
        hfhome=(ImageView)findViewById(R.id.fhome);
        finfo=(ImageView)findViewById(R.id.finfo);
        fcart=(ImageView)findViewById(R.id.fcart);
        flist=(ImageView)findViewById(R.id.flistorder);
        fprofile=(ImageView)findViewById(R.id.fprofile);
        fwallet=(ImageView)findViewById(R.id.fwallet);
        referfriendtxt=findViewById(R.id.referfriend);
        fprofile.setImageResource(R.drawable.ic_user_selected);
//        fprofile.setImageResource(R.drawable.pro);

        JSONObject obj=new JSONObject();
        try {
            obj.put("userId", StaticInfo.userId);
            requestJSON(String.valueOf(obj));
            Log.d("initializeuserId",""+obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        hfhome.setOnClickListener(this);
        finfo.setOnClickListener(this);
        fcart.setOnClickListener(this);
        flist.setOnClickListener(this);
        fprofile.setOnClickListener(this);
        confirmtxt.setOnClickListener(this);
        fwallet.setOnClickListener(this);
        referfriendtxt.setOnClickListener(this);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.pedit:
                pEdit();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void pEdit() {
        if (confirmtxt. getVisibility() == VISIBLE){
            phonetxt.setEnabled(false);
            nametxt.setEnabled(false);
            emailtxt.setEnabled(false);
            confirmtxt.setVisibility(View.GONE);
        }else{
            phonetxt.setEnabled(true);
            nametxt.setEnabled(true);
            emailtxt.setEnabled(true);
            confirmtxt.setVisibility(VISIBLE);
        }

    }

    private void requestJSON(String response) {

        final LoadingDialog loadingDialog=new LoadingDialog(this);
        loadingDialog.startLoadingDialog();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
//            String URL = "http://...";
//            JSONObject jsonBody = new JSONObject(response);
        final String requestBody = response;
        Log.d("requestjsondfdgf", String.valueOf(response));


        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.MAIN_URL+Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("strrrrsdfr", ">>" + response);
                loadingDialog.dismissDialog();
//
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error")==true){
                        Log.d("signin response", ">>" + "error");
                    }else if (obj.getBoolean("error")==false){
                        String msg=obj.getString("message");
//                        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
                        JSONObject dataobj = new JSONObject(String.valueOf(obj.getJSONObject("data")));
                        nametxt.setText(""+dataobj.getString("custName"));
                        phonetxt.setText(""+dataobj.getString("custPhone"));
                        emailtxt.setText(""+dataobj.getString("custEmail"));
                        referCode=dataobj.getString("refCode");
                        phonetxt.setEnabled(false);
                        nametxt.setEnabled(false);
                        emailtxt.setEnabled(false);
                    }
//                        obj.getInt()

                } catch (JSONException e) {
                    e.printStackTrace();
                }
//

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }


        };

        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View view) {
        // preventing double, using threshold of 1000 ms
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
            return;
        }

        lastClickTime = SystemClock.elapsedRealtime();
        if (view == hfhome) {
            Intent intent = new Intent(this, HomeScreen.class);
            startActivity(intent);
        } else if (view == fcart) {
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);
        } else if (view == finfo) {
            Intent i = new Intent(this, AboutUs.class);
            startActivity(i);
        } else if (view == flist) {
            Intent i = new Intent(this, OrderListActivity.class);
            startActivity(i);
        } else if (view == fprofile) {
            if (StaticInfo.userId != 0) {
                Intent i = new Intent(this, ProfileActivity.class);
                startActivity(i);
            } else {
                Intent i = new Intent(this, SignInActicity.class);
                startActivity(i);
            }

        }else if (view==confirmtxt){

            JSONObject requstbody = new JSONObject();
                String nameStr,mobileStr,emailStr;
                nameStr=nametxt.getText().toString();
                mobileStr=phonetxt.getText().toString();
                emailStr=emailtxt.getText().toString();
            try {
                requstbody.put("userId",String.valueOf(session.getUserId()));
                requstbody.put("name",nameStr);
                requstbody.put("mobile",mobileStr);
                requstbody.put("email",emailStr);

                updateProfilerequestJSON(String.valueOf(requstbody));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else if (view == fwallet) {
            Intent intent = new Intent(this, WalletHistory.class);
            startActivity(intent);
        }else if (view == referfriendtxt) {
            Intent intent = new Intent(this, ReferalActivity.class);
            intent.putExtra("referalCode",referCode);
            startActivity(intent);
        }



    }

    private void updateProfilerequestJSON(String response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
//            String URL = "http://...";
//            JSONObject jsonBody = new JSONObject(response);
        final String requestBody = response;
        Log.d("requestjsondfdgf", String.valueOf(response));


        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.MAIN_URL+updateUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("strrrrsdfr", ">>" + response);
//
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error")==true){
                        String msg=obj.getString("message");
                        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
                        Log.d("signin response", ">>" + "error");
                    }else if (obj.getBoolean("error")==false){
                        String msg=obj.getString("message");
                        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();


                        phonetxt.setEnabled(false);
                        nametxt.setEnabled(false);
                        emailtxt.setEnabled(false);
                        confirmtxt.setVisibility(View.GONE);
                    }
//                        obj.getInt()

                } catch (JSONException e) {
                    e.printStackTrace();
                }
//

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }


        };

        requestQueue.add(stringRequest);
    }
    public void setAppLocale(String localeCode){
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
//        prefData.setCurrentLanguage(localeCode);
        editor.putString("My_Lang","ar");
        editor.apply();
        res.updateConfiguration(conf,dm);
    }

}