package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.shahe.R;
import com.lia.shahe.utility.Session;
import com.lia.shahe.utility.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

public class SignInActicity extends AppCompatActivity implements
        View.OnClickListener {

    private TextView signBtn,forgotbtn,signupBtn;
    private EditText cname,cPassword;
    private String URLstring="customerlogin";
    private Session session;
    private long lastClickTime = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        session = new Session(this);
//            if (session.getUserId()!=0){
//                Intent intent=new Intent(getApplicationContext(), BranchActivity.class);
//                startActivity(intent);
//            }
//        session.getUserId();
        Log.d("suid",""+session.getUserId());


        signBtn=(TextView)findViewById(R.id.signinbtn);
        forgotbtn=(TextView)findViewById(R.id.forget);
        signupBtn=(TextView)findViewById(R.id.signupbtn);
        cname=(EditText)findViewById(R.id.cname);
        cPassword=(EditText)findViewById(R.id.cPassword);
        signBtn.setOnClickListener(this);
        signupBtn.setOnClickListener(this);
        forgotbtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

//        Log.d("clickmethod","dfghjkl;fcghjfghjkgv");
        // preventing double, using threshold of 1000 ms
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000){
            return;
        }

        lastClickTime = SystemClock.elapsedRealtime();

        if (view==signBtn){
//            Toast.makeText(this,"zzzzzzz",LENGTH_LONG).show();
            String cnameStr= String.valueOf(cname.getText());
            String cpswdStr= String.valueOf(cPassword.getText());
            JSONObject requstbody = new JSONObject();
            try {
                requstbody.put("phone","966"+cnameStr);
                requstbody.put("password",cpswdStr);

                requestJSON(requstbody);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else if (view==signupBtn){

            Intent intent=new Intent(getApplicationContext(),SignupMobile.class);
            startActivity(intent);

        }else if (view==forgotbtn){
            Intent intent=new Intent(getApplicationContext(),ForgotPassword.class);
            startActivity(intent);
        }

    }

    private void requestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        final String requestBody = response.toString();
        Log.d("requestjsondfdgf", String.valueOf(response));


        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.MAIN_URL+URLstring, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("strrrrsdfr", ">>" + response);
//
                try {

                    JSONObject obj = new JSONObject(response);
                   if (obj.getBoolean("error")==true){
                       String msg=obj.getString("message");
                       Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
                       Log.d("signin response", ">>" + "error");
                    }else if (obj.getBoolean("error")==false){
                       String msg=obj.getString("message");
                       Toast.makeText(getApplicationContext(),getString(R.string.Valid_User),Toast.LENGTH_SHORT).show();
                       JSONObject dataobj = new JSONObject(String.valueOf(obj.getJSONObject("data")));
                       StaticInfo.userId= Integer.parseInt(dataobj.getString("userID"));
                       session.setUserId(Integer.parseInt(dataobj.getString("userID")));
                       Log.d("zxcvuId", String.valueOf(session.getUserId()));
                       Intent intent=new Intent(getApplicationContext(),BranchActivity.class);
                       startActivity(intent);


                   }
//                        obj.getInt()

                } catch (JSONException e) {
                    e.printStackTrace();
                }
//

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);


    }
    @Override
    public void onBackPressed() {

        finish();
        Intent intent = new Intent(SignInActicity.this, HomeScreen.class);
        startActivity(intent);
    }

    public void setAppLocale(String localeCode){
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
//        prefData.setCurrentLanguage(localeCode);
        editor.putString("My_Lang","ar");
        editor.apply();
        res.updateConfiguration(conf,dm);
    }

}