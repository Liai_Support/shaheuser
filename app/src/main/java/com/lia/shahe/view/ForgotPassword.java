package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.shahe.R;
import com.lia.shahe.utility.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

public class ForgotPassword extends AppCompatActivity implements View.OnClickListener {

    private EditText emailtext;
    private TextView sbmtbtn;
    private long lastClickTime = 0;
    private String Url="checkphoneno";
    private ImageView hfhome,finfo,fcart,flist,fprofile,fwallet;
    private String  mobileStr;

    private String sendotp = "sendotp";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        hfhome=(ImageView)findViewById(R.id.fhome);
        finfo=(ImageView)findViewById(R.id.finfo);
        fcart=(ImageView)findViewById(R.id.fcart);
        flist=(ImageView)findViewById(R.id.flistorder);
        fprofile=(ImageView)findViewById(R.id.fprofile);
        fwallet=(ImageView)findViewById(R.id.fwallet);

        emailtext=(EditText)findViewById(R.id.emailtxt);
        sbmtbtn=(TextView)findViewById(R.id.btntxt);
        sbmtbtn.setOnClickListener(this);
        hfhome.setOnClickListener(this);
        finfo.setOnClickListener(this);
        fcart.setOnClickListener(this);
        flist.setOnClickListener(this);
        fprofile.setOnClickListener(this);
        fwallet.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000){
            return;
        }

        lastClickTime = SystemClock.elapsedRealtime();
        if (view==sbmtbtn){
             mobileStr= String.valueOf(emailtext.getText());
            if (mobileStr!=null|| !mobileStr.equals("")) {
                JSONObject obj=new JSONObject();
                try {
                    obj.put("phone", "966"+mobileStr);
                    checkMobileNo(String.valueOf(obj));
                    Log.d("checkMobileNo",""+obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }else{
                Toast.makeText(this,"Enter Mobile No",Toast.LENGTH_LONG).show();
            }



        }
        else if (view == hfhome) {
            Intent intent = new Intent(this, HomeScreen.class);
            startActivity(intent);
        }

        else if (view==fcart){
            Intent intent = new Intent(this, CartActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }else if (view==finfo){
            Intent i=new Intent(this,AboutUs.class);
            startActivity(i);
        }else if (view==flist){
            Intent i=new Intent(this,OrderListActivity.class);
            startActivity(i);
        }else if (view==fwallet){
            Intent i=new Intent(this,WalletHistory.class);
            startActivity(i);
        }
        else if (view==fprofile){
            if (StaticInfo.userId!=0) {
                Intent i = new Intent(this, ProfileActivity.class);
                startActivity(i);
            }else {
                Intent i = new Intent(this, SignInActicity.class);
                startActivity(i);
            }
        }
    }
    private void checkMobileNo(String response) {

        final LoadingDialog loadingDialog=new LoadingDialog(this);
        try {
            loadingDialog.startLoadingDialog();
            RequestQueue requestQueue = Volley.newRequestQueue(this);
//            String URL = "http://...";
            JSONObject jsonBody = new JSONObject(response);

            final String requestcartBody = jsonBody.toString();
            Log.d("requestjsondfdgf", String.valueOf(StaticInfo.MAIN_URL+Url));
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL_V3+Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loadingDialog.dismissDialog();

//
                    try {

                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error").equals(true) || obj.getBoolean("error") ==true){
                            Log.d("strrrrsdfr", ">>" + response);
                            JSONObject obj1=new JSONObject();
                            try {
                                obj1.put("phone", mobileStr);
                                sendotp(String.valueOf(obj1));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else{

                            Toast.makeText(getApplicationContext(),obj.getString("message"),Toast.LENGTH_SHORT);

                        }
//                        obj.getInt()
                        Log.d("strrrrsddfgsdfgsdfr", ">>" + response);



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }


            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void requestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
//            String URL = "http://...";
//            JSONObject jsonBody = new JSONObject(response);
        final String requestBody = response.toString();
        Log.d("requestjsondfdgf", String.valueOf(response));


        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.MAIN_URL+Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("strrrrsdfr", ">>" + response);
//
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error")==true){
                        String msg=obj.getString("message");
                        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
                        Log.d("signin response", ">>" + "error");
                    }else if (obj.getBoolean("error")==false){
                        String msg=obj.getString("message");
//                        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
                        TextView content = new TextView(ForgotPassword.this);
                        final Typeface typeface = ResourcesCompat.getFont(ForgotPassword.this, R.font.droidkufiregular);
                        content.setMaxLines(3);
                        content.setPadding(15,15,15,15);

                        content.setText(getString(R.string.new_password_sent)+""+getString(R.string.please_check_your));
                        content.setTypeface(typeface);
                        Context context = new ContextThemeWrapper(ForgotPassword.this,R.style.CustomFontTheme);
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle(getString(R.string.confirm));
//                        builder.setMessage(getString(R.string.new_password_sent)+""+getString(R.string.please_check_your));
                        builder.setView(content);
                        builder.setCancelable(false);

                        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                startActivity(new Intent(ForgotPassword.this, SignInActicity.class));
                            }
                        });

                        builder.show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);


    }
    @Override
    public void onBackPressed() {

        finish();
        Intent intent = new Intent(ForgotPassword.this, SignInActicity.class);
        startActivity(intent);
    }

    public void setAppLocale(String localeCode){
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
//        prefData.setCurrentLanguage(localeCode);
        editor.putString("My_Lang","ar");
        editor.apply();
        res.updateConfiguration(conf,dm);
    }

    private void sendotp(String response) {

        final LoadingDialog loadingDialog=new LoadingDialog(this);
        try {
            loadingDialog.startLoadingDialog();
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(response);

            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL_V3+sendotp, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loadingDialog.dismissDialog();
                    Log.d("strrrrsdfr", ">>" + response);
                    try {

                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error").equals(true) || obj.getBoolean("error")==true){

                            Toast.makeText(getApplicationContext(),obj.getString("message"),Toast.LENGTH_SHORT).show();

                        }else{
                            JSONObject dataobj=obj.getJSONObject("data");
                            Intent intent = new Intent(ForgotPassword.this, VerifyPhoneActivity.class);
                            intent.putExtra("phoneNo", mobileStr);
                            intent.putExtra("action", "forgot");
                            intent.putExtra("otp",dataobj.getString("otp"));
                            startActivity(intent);

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }


            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




}