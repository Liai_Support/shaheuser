package com.lia.shahe.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;

import com.lia.shahe.R;

class LoadingDialog {

     private Activity activity;
     private AlertDialog dialog;

     LoadingDialog(Activity myActivity){
         this.activity=myActivity;

     }



    void startLoadingDialog(){
         AlertDialog.Builder builder=new AlertDialog.Builder(activity);
         LayoutInflater inflater=activity.getLayoutInflater();
         builder.setView(inflater.inflate(R.layout.custom_dialog,null));

         builder.setCancelable(false);
         dialog=builder.create();
         dialog.show();
        dialog.getWindow().setLayout(600, 600);
//        dialog.setInverseBackgroundForced(true);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

     }
     void dismissDialog(){
         dialog.dismiss();
     }
}
