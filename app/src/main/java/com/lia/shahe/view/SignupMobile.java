package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.shahe.R;
import com.lia.shahe.utility.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class SignupMobile extends AppCompatActivity implements View.OnClickListener {
    private EditText mobiletxt;
    private TextView sbmt;
    private String Url ="checkphoneno";
    private String sendotp = "sendotp";
    private  String mobileStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_mobile);
        mobiletxt=findViewById(R.id.regmobile);
        sbmt=findViewById(R.id.sbmtbtn);
        sbmt.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view==sbmt){
             mobileStr=mobiletxt.getText().toString();
            String phoneNumber="+91"+mobileStr;
            if (mobileStr!=null|| !mobileStr.equals("")) {
                JSONObject obj=new JSONObject();
                try {
                    obj.put("phone", "966"+mobileStr);
                    checkMobileNo(String.valueOf(obj));
                    Log.d("checkMobileNo",""+obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }else{
                Toast.makeText(this,"Enter Mobile No",Toast.LENGTH_LONG).show();
            }
        }
    }

    private void checkMobileNo(String response) {

        final LoadingDialog loadingDialog=new LoadingDialog(this);
        try {
            loadingDialog.startLoadingDialog();
            RequestQueue requestQueue = Volley.newRequestQueue(this);
//            String URL = "http://...";
            JSONObject jsonBody = new JSONObject(response);

            final String requestcartBody = jsonBody.toString();
            Log.d("requestjsondfdgf", String.valueOf(StaticInfo.MAIN_URL+Url));
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL_V3+Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loadingDialog.dismissDialog();
                    Log.d("strrrrsdfr", ">>" + response);
//
                    try {

                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error").equals(true) || obj.getBoolean("error")==true){

                            Toast.makeText(getApplicationContext(),obj.getString("message"),Toast.LENGTH_SHORT).show();

                        }else{

                            JSONObject obj1=new JSONObject();
                            try {
                                obj1.put("phone", mobileStr);
                                sendotp(String.valueOf(obj1));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }


            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void sendotp(String response) {

        final LoadingDialog loadingDialog=new LoadingDialog(this);
        try {
            loadingDialog.startLoadingDialog();
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(response);

            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL_V3+sendotp, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loadingDialog.dismissDialog();
                    Log.d("strrrrsdfr", ">>" + response);
                    try {

                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error").equals(true) || obj.getBoolean("error")==true){

                            Toast.makeText(getApplicationContext(),obj.getString("message"),Toast.LENGTH_SHORT).show();

                        }else{
                            JSONObject dataobj=obj.getJSONObject("data");
                            Intent intent = new Intent(SignupMobile.this, VerifyPhoneActivity.class);
                            intent.putExtra("phoneNo", mobileStr);
                            intent.putExtra("action", "signup");
                            intent.putExtra("otp",dataobj.getString("otp"));
                            startActivity(intent);

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }


            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}