package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.shahe.R;
import com.lia.shahe.bo.CuttingStyle;
import com.lia.shahe.bo.KheemaDetails;
import com.lia.shahe.bo.PackingStyle;
import com.lia.shahe.bo.ProductDetailsBo;
//import com.lia.shahe.notification.NotificationCountSetClass;
import com.lia.shahe.utility.Session;
import com.lia.shahe.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Locale;

import static java.lang.Double.sum;

public class ProductDetails extends AppCompatActivity implements
        View.OnClickListener {

    TextView textCartItemCount;
    int mCartItemCount = 0;

    //    ArrayList<String> productSize;
    private String URLstring= "getproductdetails";
    private String URLcartstring= "addtocart";
    private Spinner pspinner,psspinner,kheemaspinner,csspinner;
    private ArrayList<ProductDetailsBo> pdetails;
    private ArrayList<CuttingStyle> csdetails;
    private ArrayList<KheemaDetails> kdetails;
    private ArrayList<PackingStyle> psdetails;
    private ArrayList<String> prodprice = new ArrayList<String>();
    private ArrayList<String> csdet = new ArrayList<String>();
    private ArrayList<String> kdet = new ArrayList<String>();
    private ArrayList<String> pdet = new ArrayList<String>();
    private JsonArrayRequest prequest,brequest;
    private RequestQueue prequestQueue,brequestQueue;
    private TextView qtytext,cart_btn;
    private ImageView minusbtn,plusbtn;
    private Double pprice,kdprice,oprice,kheemaPrice,sPrice,sKheema;
    private int Sqty;
    private EditText pricetext,notetxt;
    private  String kprice,proprice,totprice;
    private int qty=1;
    public static int notificationCountCart = 0;
    private Session session;
    private long lastClickTime = 0;
    private int pdetId,kheemaId,csId,spackingId,userId,productId,branchId;
    private String noteStr,asd;


    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        session = new Session(this);

        kprice=String.valueOf(0);
        Log.d("sdfsdfghjk", String.valueOf(productId));
        userId= StaticInfo.userId;

        Spinner spin = (Spinner) findViewById(R.id.kgspinner);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_arrow));

//        toolbar.setLogo(getResources().getDrawable(R.drawable.ic_meat_app_logo));


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                Intent intent = new Intent(getBaseContext(), HomeScreen.class);
                startActivity(intent);
            }
        });

        JSONObject obj=new JSONObject();
        Log.d("asdfgh",""+getIntent().getStringExtra("catId"));
        productId= Integer.parseInt(getIntent().getStringExtra("catId"));
        Log.d("asdfgh",""+productId);
        try {
            obj.put("product",productId);
            obj.put("branch", session.getBranchId());
            retrieveJSON(String.valueOf(obj));

            Log.d("initializeuserId",""+productId+","+session.getBranchId());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        pspinner=(Spinner)findViewById(R.id.kgspinner);
        kheemaspinner=(Spinner)findViewById(R.id.kheemaspinner);
        csspinner=(Spinner)findViewById(R.id.csspinner);
        psspinner=(Spinner)findViewById(R.id.packingspinner);
        qtytext=(TextView)findViewById(R.id.qtytext);
        notetxt=(EditText)findViewById(R.id.notes);
        notetxt.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (notetxt.hasFocus()) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK){
                        case MotionEvent.ACTION_SCROLL:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            return true;
                    }
                }
                return false;
            }
        });




//        notetxt.setInputType(InputType.TYPE_CLASS_TEXT);
//        notetxt.setImeOptions(EditorInfo.IME_ACTION_DONE);
//        notetxt.setRawInputType(InputType.TYPE_CLASS_TEXT);

        plusbtn=(ImageView) findViewById(R.id.plusbtn);
        minusbtn=(ImageView) findViewById(R.id.minusbtn);
        cart_btn=(TextView)findViewById(R.id.cart_btn);
        pricetext=(EditText)findViewById(R.id.pprice);
        minusbtn.setOnClickListener(this);
        plusbtn.setOnClickListener(this);
        cart_btn.setOnClickListener(this);

        pspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                final ProductDetailsBo listData=pdetails.get(i);
                pdetId=listData.getProductDetailsId();
                pprice=Double.parseDouble(listData.getpPrice());
                Double oprice = qty * Double.valueOf(pprice);
                proprice=String.valueOf(pprice);

                Double khprice=0.0;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    khprice= sum(oprice, Double.parseDouble(kprice));

                    pricetext.setText(""+khprice);
                }



            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });
        kheemaspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                final KheemaDetails kheemaData=kdetails.get(i);
                int kqty=Integer.parseInt(qtytext.getText().toString());
               Double toprice=Double.parseDouble(pricetext.getText().toString());
                kdprice= Double.valueOf(kheemaData.getkPrice());
                Double oprice = qty * Double.valueOf(proprice);

                kprice= String.valueOf(kdprice);
                Log.d("kmprice",""+kprice);
                kheemaPrice= kdprice;


                kheemaId=kheemaData.getkId();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    kheemaPrice=sum(kheemaPrice,oprice);
                }else{
                    kheemaPrice=kheemaPrice+oprice;
                }
                pricetext.setText(""+kheemaPrice);

                String country1=   kheemaspinner.getItemAtPosition(kheemaspinner.getSelectedItemPosition()).toString();

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
                kprice= String.valueOf(0);
            }
        });
        csspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String country1=   csspinner.getItemAtPosition(csspinner.getSelectedItemPosition()).toString();
                final CuttingStyle csData=csdetails.get(i);
                csId=csData.getcId();

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });
        psspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String country1=   psspinner.getItemAtPosition(psspinner.getSelectedItemPosition()).toString();
                final PackingStyle psData=psdetails.get(i);
                spackingId=psData.getpId();

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

    }

    ///////////////////////menu start/////////////////////////
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);

        final MenuItem menuItem = menu.findItem(R.id.cart);

        View actionView = menuItem.getActionView();
        textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);

        setupBadge();

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.cart: {
                // Do something
                cartAction();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
    private void cartAction() {
        Intent i=new Intent(this,CartActivity.class);
        startActivity(i);
    }

    private void setupBadge() {

        if (textCartItemCount != null) {
            if (StaticInfo.cartCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(StaticInfo.cartCount, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }


////////////////menu end///////////////
///////////////////////menu end ////////////////////////



    private void retrieveJSON(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(this);

        try {
            loadingDialog.startLoadingDialog();
            RequestQueue requestQueue = Volley.newRequestQueue(this);
//            String URL = "http://...";
            JSONObject jsonBody = new JSONObject(response);

            final String requestBody = jsonBody.toString();


            StringRequest stringRequest = new StringRequest(Request.Method.POST,StaticInfo.MAIN_URL+URLstring, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loadingDialog.dismissDialog();
                    Log.d("strrrrr", ">>" + response);
//
                    try {

                        JSONObject checkresobj = new JSONObject(response);
                        Log.d("asdf",checkresobj.getString("error"));
                        if (checkresobj.getString("error")=="false") {
                            JSONObject obj = checkresobj.getJSONObject("data");

                            Log.d("id", String.valueOf(obj));
                            /////////////////////////////cuttingStyle///////////////
                            pdetails = new ArrayList<>();
                            JSONArray dataArray = obj.getJSONArray("ProductDetails");
                            Log.d("id", String.valueOf(dataArray));
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataobj = dataArray.getJSONObject(i);
                                ProductDetailsBo productDetail = new ProductDetailsBo(dataobj.getInt("id"), dataobj.getString("desc"), dataobj.getString("price"));
                                pdetails.add(productDetail);

                            }

//                            new SpinnerVO(0, "King in the north")
                            for (int i = 0; i < pdetails.size(); i++) {

                                prodprice.add(pdetails.get(i).getpDesc().toString() + "(" + pdetails.get(i).getpPrice().toString() + ")");
                            }

                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(ProductDetails.this,R.layout.spinner_layout, prodprice);
                            spinnerArrayAdapter.setDropDownViewResource(R.layout.dropdown_view_layout); // The drop down view
                            pspinner.setAdapter(spinnerArrayAdapter);
//                            pspinner.setSelection(prodprice.size() - 1);
                            /////////////////////////////ProductDetails End///////////////

                            /////////////////////////////cuttingStyle///////////////
                            csdetails = new ArrayList<>();
                            JSONArray csdataArray = obj.getJSONArray("cuttingStyle");
                            Log.d("pid", String.valueOf(csdataArray));
                            for (int i = 0; i < csdataArray.length(); i++) {
                                JSONObject csdataobj = csdataArray.getJSONObject(i);
//                                Log.d("piffd", String.valueOf(csdataobj.getInt("cId")));

                                CuttingStyle cstyleDetail = new CuttingStyle(csdataobj.getInt("id"), csdataobj.getString("desc"));
                                csdetails.add(cstyleDetail);

                            }
//                            Log.d("sd", String.valueOf(csdetails.size()));
                            for (int i = 0; i < csdetails.size(); i++) {
                                csdet.add(csdetails.get(i).getcName().toString());
                            }
                            ArrayAdapter<String> csspinnerArrayAdapter = new ArrayAdapter<String>(ProductDetails.this, R.layout.spinner_layout, csdet);
                            csspinnerArrayAdapter.setDropDownViewResource(R.layout.dropdown_view_layout); // The drop down view
                            csspinner.setAdapter(csspinnerArrayAdapter);

                            /////////////////////////////cuttingStyle End///////////////
                            /////////////////////////////kheemaDetails///////////////
                            kdetails = new ArrayList<>();
                            JSONArray kheemadataArray = obj.getJSONArray("kheemaDetails");
                            Log.d("pid", String.valueOf(kheemadataArray));
                            for (int i = 0; i < kheemadataArray.length(); i++) {
                                JSONObject kheemadataobj = kheemadataArray.getJSONObject(i);
//                                Log.d("piffd", String.valueOf(kheemadataobj.getInt("kId")));

                                KheemaDetails kheemaDetail = new KheemaDetails(kheemadataobj.getInt("id"), kheemadataobj.getString("desc"), kheemadataobj.getString("price"));
                                kdetails.add(kheemaDetail);

                            }
//                            Log.d("sd", String.valueOf(kdetails.size()));
                            for (int i = 0; i < kdetails.size(); i++) {
//                                if (i==0) {
//                                    kdet.add(kdetails.get(i).getkDesc().toString());
//                                }else {
                                kdet.add(kdetails.get(i).getkDesc().toString() + "(" + kdetails.get(i).getkPrice().toString() + ")");
//                                }

                            }
                            ArrayAdapter<String> kheemaspinnerArrayAdapter = new ArrayAdapter<String>(ProductDetails.this, R.layout.spinner_layout, kdet);
                            kheemaspinnerArrayAdapter.setDropDownViewResource(R.layout.dropdown_view_layout); // The drop down view
                            kheemaspinner.setAdapter(kheemaspinnerArrayAdapter);

                            /////////////////////////////kheemaDetails End///////////////

                            /////////////////////////////packingStyle///////////////
                            psdetails = new ArrayList<>();
                            JSONArray psdataArray = obj.getJSONArray("packingStyle");
                            Log.d("pid", String.valueOf(psdataArray));
                            for (int i = 0; i < psdataArray.length(); i++) {
                                JSONObject psdataobj = psdataArray.getJSONObject(i);
                                PackingStyle psDetail = new PackingStyle(psdataobj.getInt("id"), psdataobj.getString("desc"));
                                Log.d("piffd", String.valueOf(psDetail));
                                psdetails.add(psDetail);

                            }

                            for (int i = 0; i < psdetails.size(); i++) {
                                Log.d("sd", String.valueOf(psdetails.get(i).getpName().toString()));
                                pdet.add(psdetails.get(i).getpName().toString());
                            }
                            ArrayAdapter<String> psspinnerArrayAdapter = new ArrayAdapter<String>(ProductDetails.this, R.layout.spinner_layout, pdet);
                            psspinnerArrayAdapter.setDropDownViewResource(R.layout.dropdown_view_layout); // The drop down view
                            psspinner.setAdapter(psspinnerArrayAdapter);

                            /////////////////////////////packingStyle End///////////////
                        }else if(checkresobj.getString("error")=="true"){
                            TextView content = new TextView(ProductDetails.this);
                            final Typeface typeface = ResourcesCompat.getFont(ProductDetails.this, R.font.droidkufiregular);
                            content.setMaxLines(3);
                            content.setPadding(15,15,15,15);

                            content.setText(getString(R.string.no_products_found));
                            content.setTypeface(typeface);
                            Context context = new ContextThemeWrapper(ProductDetails.this,R.style.CustomFontTheme);
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle(getString(R.string.alert));
                            builder.setView(content);
//                            builder.setMessage(getString(R.string.no_products_found));
                            builder.setCancelable(false);
                            builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
//                     session.logoutUser();
//                                    StaticInfo.userId = 0;
//                                    StaticInfo.cartCount = 0;
//                                    session.setUserId(0);
                                    startActivity(new Intent(ProductDetails.this, HomeScreen.class));
                                }
                            });
                            builder.show();
//                         Toast.makeText(getApplicationContext(),checkresobj.getString("message"),Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }


            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void cartJSON( String response) {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
//            String URL = "http://...";
            JSONObject jsonBody = new JSONObject(response);

            final String requestcartBody = jsonBody.toString();
            Log.d("requestjsondfdgf", String.valueOf(jsonBody));


            StringRequest stringRequest = new StringRequest(Request.Method.POST,StaticInfo.MAIN_URL+URLcartstring, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("strrrrsdfr", ">>" + response);
//
                    try {

                        JSONObject obj = new JSONObject(response);
                        if (obj.getString("error")=="true"){

//                            Toast.makeText(getApplicationContext(),obj.getString("message"),Toast.LENGTH_SHORT).show();

                        }else if(obj.getString("error")=="false"){
                            JSONObject dataobj=new JSONObject(obj.getString("data"));
                            StaticInfo.cartCount= Integer.parseInt(dataobj.getString("cartCount"));
                            Intent i=new Intent(getApplicationContext(),CartActivity.class);
                            startActivity(i);

                        }




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }


            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == minusbtn) {
            if (qty>1) {
                qty = qty - 1;
                qtytext.setText(String.valueOf(qty));
                Double oprice = qty * Double.valueOf(pprice);
                Double khprice=0.0;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    khprice= sum(oprice, Double.parseDouble(kprice));
                    Log.d("plusprice",""+qty+"*"+proprice+","+oprice+"+"+kprice+"="+khprice+"=="+sum(oprice,kheemaPrice));
                }else {
                    khprice=oprice+Double.parseDouble(kprice);
                }
                pricetext.setText("" + khprice);
            }

        }else if (view == plusbtn){
            qty=qty+1;
            qtytext.setText(String.valueOf(qty));
            Double oprice = qty * Double.valueOf(proprice);

            Double khprice=0.0;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                khprice= sum(oprice, Double.parseDouble(kprice));
                Log.d("plusprice",""+qty+"*"+proprice+","+oprice+"+"+kprice+"="+khprice+"=="+sum(oprice,kheemaPrice));
                pricetext.setText(""+khprice);
            }


        }else if (view == cart_btn) {
            // preventing double, using threshold of 1000 ms
            if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                return;
            }

            lastClickTime = SystemClock.elapsedRealtime();

            noteStr=notetxt.getText().toString();
            byte[] data2 = new byte[0];
            try {
                data2 = noteStr.getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            asd = Base64.encodeToString(data2, Base64.DEFAULT);
Log.d("asdfghjkhg",""+asd);

//            Toast.makeText(this,""+asd,Toast.LENGTH_LONG).show();
            if (StaticInfo.userId != 0) {
                // preventing double, using threshold of 1000 ms
                StaticInfo.cartCount++;
                int kg = pspinner.getSelectedItemPosition();
                int cs = csspinner.getSelectedItemPosition();
                int ps = psspinner.getSelectedItemPosition();
                int ks = kheemaspinner.getSelectedItemPosition();
                final ProductDetailsBo listData = pdetails.get(kg);
                final CuttingStyle listcsData = csdetails.get(cs);
                final PackingStyle listpsData = psdetails.get(ps);
                final KheemaDetails listkhData = kdetails.get(ks);
                Log.d("asdfghj",notetxt.getText().toString().trim());


                try {

                    JSONObject productDetailsbody = new JSONObject();
                    productDetailsbody.put("id", listData.getProductDetailsId());
                    productDetailsbody.put("desc", listData.getpDesc());
                    productDetailsbody.put("price", listData.getpPrice());
                    JSONObject cuttingstylebody = new JSONObject();
                    cuttingstylebody.put("id", listcsData.getcId());
                    cuttingstylebody.put("desc", listcsData.getcName());
                    JSONObject packingstylebody = new JSONObject();
                    packingstylebody.put("id", listpsData.getpId());
                    packingstylebody.put("desc", listpsData.getpName());
                    JSONObject kheemabody = new JSONObject();
                    kheemabody.put("id", listkhData.getkId());
                    kheemabody.put("desc", listkhData.getkDesc());
                    kheemabody.put("price", listkhData.getkPrice());
                    JSONObject cartdeatailsbody = new JSONObject();
                    cartdeatailsbody.put("userId", "" + userId);
                    cartdeatailsbody.put("productId", "" + getIntent().getStringExtra("catId"));
                    cartdeatailsbody.put("productname", "" + getIntent().getStringExtra("catName"));
                    cartdeatailsbody.put("pimage", "" + getIntent().getStringExtra("catImg"));
                    cartdeatailsbody.put("qty", "" + qty);
                    cartdeatailsbody.put("branchId", "" + session.getBranchId());
                    cartdeatailsbody.put("branchName", "" + session.getBranchAddr());
                    cartdeatailsbody.put("notes", "" + asd);
                    cartdeatailsbody.put("productDetails", productDetailsbody);
                    cartdeatailsbody.put("cuttingStyle", cuttingstylebody);
                    cartdeatailsbody.put("packingStyle", packingstylebody);
                    cartdeatailsbody.put("kheemaDetails", kheemabody);
                    cartJSON(String.valueOf(cartdeatailsbody));

//                    ProgressDialog progressBar = new ProgressDialog(this);
//                    progressBar.setCancelable(true);//you can cancel it by pressing back button
//                    progressBar.setMessage("File downloading ...");
//                    progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//                    progressBar.setProgress(0);//initially progress is 0
//                    progressBar.setMax(100);//sets the maximum value 100
//                    progressBar.show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
            else{
                TextView content = new TextView(this);
                final Typeface typeface = ResourcesCompat.getFont(this, R.font.droidkufiregular);
                content.setMaxLines(3);
                content.setPadding(15,15,15,15);

                content.setText(getString(R.string.You_need_tosign_in));
                content.setTypeface(typeface);
                Context context = new ContextThemeWrapper(this,R.style.CustomFontTheme);
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(getString(R.string.confirm));
//                builder.setMessage();
                builder.setView(content);
                builder.setCancelable(false);
                builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                     session.logoutUser();
                        StaticInfo.userId = 0;
                        StaticInfo.cartCount = 0;
                        session.setUserId(0);
                        startActivity(new Intent(ProductDetails.this, SignInActicity.class));
                    }
                });

                builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.show();
            }
        }

    }
    public void setAppLocale(String localeCode){
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
//        prefData.setCurrentLanguage(localeCode);
        editor.putString("My_Lang","ar");
        editor.apply();
        res.updateConfiguration(conf,dm);
    }

}