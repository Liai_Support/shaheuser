package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.shahe.R;
import com.lia.shahe.adapter.OrderPreviewAdapter;
import com.lia.shahe.bo.Cart;
import com.lia.shahe.bo.Coupons;
import com.lia.shahe.utility.Session;
import com.lia.shahe.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class OrderPreview extends AppCompatActivity implements View.OnClickListener{
    private static final String Url = "listcart";
    private static final String plcaeOrderUrl = "placeorder";
    private String Url1="discountcoupons";
    private List<Cart> list_cart;
    private ArrayList<Cart> arraylist;
    private List<Coupons> list_data;
    private Session session;
    private OrderPreviewAdapter orderPreviewAdapter;
    private GridLayoutManager crgm;
    private RecyclerView cartrv;
    private TextView totaltxt,plordertxt,ordertottxt,vatpertxt,subtottxt,walletamttxt,ordtotprice,applyarrowtxt,applytexttxt,removebintxt,discounttottxt;
    public static Double cartPrice= Double.valueOf(0);
    String addressID;
    private String paymentType="";
    private RadioButton paycashtxt,paycardtxt,walletbtntxt;
    private long lastClickTime = 0;
    Timer timer;
    TimerTask timerTask;
    int position;
    private LinearLayout linear6lay,discountlineartxt;
    public Double orderTotal,remainbal;
    private boolean wallet=false;
    public  Double sCorderTot=0.0;
    Button coupon;
    LinearLayout couponbox;
    EditText promocode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_preview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);

        session = new Session(this);
        this.list_cart = list_cart;
        Log.d("adressidsd",""+getIntent().getStringExtra("addressId"));
        addressID=String.valueOf(getIntent().getStringExtra("addressId"));

        promocode = (EditText) findViewById(R.id.promo_code);
        promocode.setText("");
        coupon = (Button) findViewById(R.id.coupon_btn);
        coupon.setOnClickListener(this);
        couponbox = (LinearLayout) findViewById(R.id.couponbox);
        couponbox.setVisibility(View.GONE);

        paycashtxt=(RadioButton)findViewById(R.id.paycash);
        paycardtxt=(RadioButton)findViewById(R.id.paycard);
        walletbtntxt=(RadioButton)findViewById(R.id.walletbtn);
        plordertxt=(TextView)findViewById(R.id.plorder);
        subtottxt=(TextView)findViewById(R.id.subtotval);
        vatpertxt=(TextView)findViewById(R.id.vatperval);
        ordertottxt=(TextView)findViewById(R.id.ordtotval);
        walletamttxt=(TextView)findViewById(R.id.walletamt);
        ordtotprice=findViewById(R.id.ordtotval);
        discountlineartxt=findViewById(R.id.discountlinear);
        discounttottxt=findViewById(R.id.discounttot);

        linear6lay=findViewById(R.id.linear6);
        applyarrowtxt=findViewById(R.id.applyarrow);
        applytexttxt=findViewById(R.id.applytext);
        removebintxt=findViewById(R.id.removeicon);


        if (getIntent().hasExtra( "sCorderTot")){
            sCorderTot=getIntent().getDoubleExtra("sCorderTot",0.0);
            applytexttxt.setText("تمت الموافقة");
            applytexttxt.setGravity(View.FOCUS_LEFT);
            applytexttxt.setEnabled(false);
            applyarrowtxt.setVisibility(View.GONE);
            removebintxt.setVisibility(View.VISIBLE);
            discountlineartxt.setVisibility(View.VISIBLE);
            discounttottxt.setText(""+sCorderTot+" SAR");
            promocode.setVisibility(View.GONE);
            coupon.setVisibility(View.GONE);
        }

     /*   if (StaticInfo.walletAmount<=0){
            walletamttxt.setAlpha(.5f);
            walletbtntxt.setEnabled(false);
            walletbtntxt.setAlpha(.5f);
        }else{
            walletamttxt.setAlpha(1f);
            walletbtntxt.setEnabled(true);
            walletbtntxt.setAlpha(1f);
        }*/

        walletamttxt.setText(" مبلغ المحفظة "+StaticInfo.walletAmount +" SAR");

        cartrv=(RecyclerView)findViewById(R.id.crecycler_view);
        crgm=new GridLayoutManager(this,1,GridLayoutManager.VERTICAL, false);
        cartrv.setLayoutManager(crgm);

        JSONObject obj=new JSONObject();
        try {
            obj.put("userId", StaticInfo.userId);
            getCartValues(String.valueOf(obj));
            Log.d("initializeuserId",""+obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        plordertxt.setOnClickListener(this);
        paycardtxt.setOnClickListener(this);
        paycashtxt.setOnClickListener(this);
        walletbtntxt.setOnClickListener(this);
        applytexttxt.setOnClickListener(this);
        applyarrowtxt.setOnClickListener(this);
        removebintxt.setOnClickListener(this);
        getcouponvalues();

    }

    private void getcouponvalues() {
        final LoadingDialog loadingDialog=new LoadingDialog(this);
        loadingDialog.startLoadingDialog();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
//            String URL = "http://...";


        final String requestcartBody = "";
        Log.d("requestjsondfdgfefr", String.valueOf(StaticInfo.MAIN_URL+Url));
        StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+Url1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loadingDialog.dismissDialog();
                        Log.d("strrrrsdregregfr", ">>" + response);
//
                        try {

                            JSONObject obj = new JSONObject(response);
                            if(obj.getString("error").equals(true)){
                                Toast.makeText(getApplicationContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT);
                            }else{


                                list_data=new ArrayList<>();
                                if (!obj.isNull("data") ) {
                                    JSONArray dataArray = obj.getJSONArray("data");
                                    for (int i = 0; i < dataArray.length(); i++) {
                                        JSONObject couponobj = dataArray.getJSONObject(i);
                                        Coupons coupons = new Coupons(couponobj.getInt("id"), couponobj.getString("code"),
                                                couponobj.getString("description"), couponobj.getDouble("amount"),
                                                couponobj.getString("percentage"));
                                        list_data.add(coupons);
                                        // setupCouponData(list_data);
                                        Log.w("sd", "" + list_data.size());
                                    }

                                }


                            }
//                        obj.getInt()



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
//


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                    return null;
                }
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);
    }


    private void getCartValues(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(this);
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(response);
            loadingDialog.startLoadingDialog();
            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        Log.d("json response",""+response);
                        loadingDialog.dismissDialog();
                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error")=="true"){
                            Toast.makeText(getApplicationContext(),getString(R.string.oops),Toast.LENGTH_SHORT).show();
                        }else if (obj.getString("error")=="false"){

                            list_cart=new ArrayList<>();
                            JSONArray dataArray=obj.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject carthobj = dataArray.getJSONObject(i);
                                Cart cart = new Cart(carthobj.getInt("id"),carthobj.getString("details"));
                                list_cart.add(cart);

                            }
                            setupCartData(list_cart);


                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }


            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void placeOrder(String response) {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(response);

            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+plcaeOrderUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        Log.d("json response",""+response);

                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error")=="true"){
                            Toast.makeText(getApplicationContext(),getString(R.string.oops),Toast.LENGTH_SHORT).show();
                        }else if (obj.getString("error")=="false"){

                            JSONObject dataobj=obj.getJSONObject("data");

                            Intent i=new Intent(getApplicationContext(),OrderConfirmation.class);
                            i.putExtra("orderId",dataobj.getString("orderId"));
                            startActivity(i);



                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }


            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void setupCartData(List<Cart> list_cart) {
        orderPreviewAdapter=new OrderPreviewAdapter(list_cart,this,session,this);

        cartrv.setAdapter(orderPreviewAdapter);
    }



    @Override
    public void onClick(View view) {
        if (view==paycashtxt){
            if (!wallet){
                paymentType="cash";
            }
            Log.d("cashh",paymentType);

        }else if(view == coupon){
            String code = String.valueOf(promocode.getText());
            Log.d("hgvgv",""+code);
            int i=0;
            while (i<list_data.size()){
                String ide = list_data.get(i).getcCode();
                Log.d("hgvgv",""+ide);
                if(ide.equals(code)){
                    Toast.makeText(this,"Coupon Valid",Toast.LENGTH_SHORT).show();
                    Double damt = list_data.get(i).getcAmt();
                    sCorderTot = damt;
                    Log.d("hgvvhjgv",""+damt);
                    discounttottxt.setText(""+sCorderTot+" SAR");
                    Intent intent=new Intent(OrderPreview.this, OrderPreview.class);
                    intent.putExtra("addressId",addressID);
                    intent.putExtra("couponId",String.valueOf(list_data.get(i).getId()));
                    intent.putExtra("sCorderTot",sCorderTot);
                    startActivity(intent);
                    break;
                }
                else {
                    Log.d("scdds","dcd");
//                     Toast.makeText(this,"Invalid Coupon",Toast.LENGTH_SHORT).show();
                    i++;
                }
            }


             /*for(int i=0;i<list_data.size();i++){
                 String ide = list_data.get(i).getcCode();
                 Log.d("hgvgv",""+ide);
                 if(ide.equals(code)){
                     couponbox.setVisibility(View.VISIBLE);
                 }
             }*/
        }
        else if (view==paycardtxt){
            if (!wallet){
                paymentType="card";
            }
            Log.d("cardhh",paymentType);
        }else if (view==applytexttxt){
            Intent i=new Intent(this,CouponActivity.class);
            i.putExtra("addressId",addressID);
            i.putExtra("couponId",0);
            i.putExtra("orderTotal",orderTotal);
            startActivity(i);

        }else if (view==applyarrowtxt){
            Intent i=new Intent(this,CouponActivity.class);
            i.putExtra("addressId",addressID);
            i.putExtra("couponId",0);
            i.putExtra("orderTotal",orderTotal);
            startActivity(i);
        }else if (view==removebintxt){

            Intent intent=new Intent(this,OrderPreview.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("addressId",addressID);
            intent.putExtra("couponId","0");
            startActivity(intent);

        } else if (view==walletbtntxt){
            if (!walletbtntxt.isSelected()) {

                walletbtntxt.setChecked(true);
                walletbtntxt.setSelected(true);
                wallet=true;
                Log.d("TAG", "onClick: "+wallet);
                String oTotatl=ordtotprice.getText().toString();
                String wAmt= ""+StaticInfo.walletAmount+" SAR";

//                 if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//                     double totprice=Double.sum(orderTotal, StaticInfo.walletAmount);
//
//
//                     Log.d("asd",""+totprice);
//                 }else {
                remainbal=orderTotal-StaticInfo.walletAmount;

                if (remainbal<=0){
                    paycashtxt.setEnabled(false);
                    paycardtxt.setEnabled(false);
                    linear6lay.setAlpha(.5f);
                    paymentType="wallet";
                }else{

                    paycashtxt.setEnabled(true);
                    paycardtxt.setEnabled(true);
                    linear6lay.setAlpha(1);
                }

//                 Log.d("TAG", "offClick: "+wallet);
//                 }



            /*     if (oTotatl.equals(wAmt)){
                     paycashtxt.setEnabled(false);
                     paycardtxt.setEnabled(false);
                     linear6lay.setAlpha(.5f);
                 }else{
                     paycashtxt.setEnabled(true);
                     paycardtxt.setEnabled(true);
                     linear6lay.setAlpha(1);
                 }*/




            } else {
                wallet=false;
                paycashtxt.setEnabled(true);
                paycardtxt.setEnabled(true);
                linear6lay.setAlpha(1);
                paycardtxt.setSelected(false);
                paycashtxt.setSelected(false);
                paycardtxt.setChecked(false);
                paycashtxt.setChecked(false);
                paymentType="";
                Log.d("TAG", "offClick: "+wallet);
                walletbtntxt.setChecked(false);
                walletbtntxt.setSelected(false);
            }
        }
        else if (view==plordertxt){
            // preventing double, using threshold of 1000 ms
            if (SystemClock.elapsedRealtime() - lastClickTime < 1000){
                return;
            }
            lastClickTime = SystemClock.elapsedRealtime();
            Log.d("paytype",paymentType);
            if (wallet){
                Double remainbalamt=orderTotal-StaticInfo.walletAmount;
                if (remainbalamt<=0){
                    paymentType="wallet";

                }else{
                    paymentType="cashwallet";



                }

            }

            if (paymentType!="") {
//                 if (paymentType.equals("cashwallet")){
                Log.d("paytype1",paymentType);
                JSONObject obj = new JSONObject();
                try {
                    obj.put("addressid", addressID);
                    obj.put("userid", StaticInfo.userId);

                    obj.put("paymentType", paymentType);


                    obj.put("couponId", getIntent().getStringExtra("couponId"));
                    placeOrder(String.valueOf(obj));
                    Log.d("initializeuserId", "" + obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }else{
                Log.d("paytype2",paymentType);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getString(R.string.alert));
                builder.setMessage(getString(R.string.choose_payment));
                builder.setCancelable(false);
                builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });


                builder.show();
            }
        }

      /*       else{
                 AlertDialog.Builder builder = new AlertDialog.Builder(this);
                 builder.setTitle(getString(R.string.alert));
                 builder.setMessage(getString(R.string.choose_payment));
                 builder.setCancelable(false);
                 builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                     @Override
                     public void onClick(DialogInterface dialog, int which) {


                     }
                 });


                 builder.show();
             }*/

    }

    @Override
    public void onBackPressed() {
        subtottxt.setText(""+0);
        cartPrice=0.0;
        finish();
        Intent intent = new Intent(OrderPreview.this, AddressList.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
    public void setAppLocale(String localeCode){
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
//        prefData.setCurrentLanguage(localeCode);
        editor.putString("My_Lang","ar");
        editor.apply();
        res.updateConfiguration(conf,dm);
    }

}