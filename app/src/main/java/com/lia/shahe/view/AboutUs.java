package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lia.shahe.R;
import com.lia.shahe.utility.Session;
import com.lia.shahe.utility.StaticInfo;

import java.util.Locale;

public class AboutUs extends AppCompatActivity implements View.OnClickListener{
    private TextView  aboutcon,aboutphone;
    private Session session;
    private ImageView hfhome,finfo,fcart,flist,fprofile,phicon,fwallet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        toolbar.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);
        hfhome=(ImageView)findViewById(R.id.fhome);
        finfo=(ImageView)findViewById(R.id.finfo);
        fcart=(ImageView)findViewById(R.id.fcart);
        flist=(ImageView)findViewById(R.id.flistorder);
        fprofile=(ImageView)findViewById(R.id.fprofile);
        fwallet=(ImageView)findViewById(R.id.fwallet);
        phicon=(ImageView)findViewById(R.id.picon);
        aboutcon=(TextView) findViewById(R.id.atxt);
        aboutphone=(TextView) findViewById(R.id.phonetxt);
        session = new Session(this);
        finfo.setImageResource(R.drawable.ic_info_selected);

        aboutcon.setText(session.getacontent());
        aboutphone.setText(session.getcontactUs());

        hfhome.setOnClickListener(this);
        finfo.setOnClickListener(this);
        fcart.setOnClickListener(this);
        flist.setOnClickListener(this);
        phicon.setOnClickListener(this);
        fprofile.setOnClickListener(this);
        fwallet.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view==hfhome){
            Intent i=new Intent(this,HomeScreen.class);
            startActivity(i);
        }else if (view==finfo){
            Intent i=new Intent(this,AboutUs.class);
            startActivity(i);
        }else if (view==flist){
            Intent i=new Intent(this,OrderListActivity.class);
            startActivity(i);
        }
        else if (view==fprofile){
            if (StaticInfo.userId!=0) {
                Intent i = new Intent(this, ProfileActivity.class);
                startActivity(i);
            }else {
                Intent i = new Intent(this, SignInActicity.class);
                startActivity(i);
            }
        }
        else if (view == fcart) {
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);
        }else if (view == fwallet) {
            Intent intent = new Intent(this, WalletHistory.class);
            startActivity(intent);
        }
        else if (view == phicon) {
            if (Build.VERSION.SDK_INT < 23) {
                phoneCall();
            }else {

                if (ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {

                    phoneCall();
                }else {
                    final String[] PERMISSIONS_STORAGE = {Manifest.permission.CALL_PHONE};
                    //Asking request Permissions
                    ActivityCompat.requestPermissions(this, PERMISSIONS_STORAGE, 9);
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        boolean permissionGranted = false;
        switch(requestCode){
            case 9:
                permissionGranted = grantResults[0]== PackageManager.PERMISSION_GRANTED;
                break;
        }
        if(permissionGranted){
            phoneCall();
        }else {
//            Toast.makeText(this, "You don't assign permission.", Toast.LENGTH_SHORT).show();
        }
    }

    private void phoneCall(){
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:"+session.getcontactUs()));
            this.startActivity(callIntent);
        }else{
//            Toast.makeText(this, "You don't assign permission.", Toast.LENGTH_SHORT).show();
        }
    }
    public void setAppLocale(String localeCode){
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
//        prefData.setCurrentLanguage(localeCode);
        editor.putString("My_Lang","ar");
        editor.apply();
        res.updateConfiguration(conf,dm);
    }

}