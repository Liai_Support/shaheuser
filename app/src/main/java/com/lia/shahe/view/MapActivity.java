package com.lia.shahe.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;

import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;



import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.RuntimeRemoteException;
import com.lia.shahe.R;
import com.lia.shahe.adapter.PlaceAutocompleteAdapter;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by User on 10/2/2017.
 */

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener
       {

           @Override
           public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

           }
           @SuppressLint("MissingPermission")
           @Override
           public void onMapReady(GoogleMap googleMap) {
               mMap = googleMap;
               mMap.setMyLocationEnabled(true);
               mMap.getUiSettings().setMyLocationButtonEnabled(true);

               mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

               if (mapView != null && mapView.findViewById(Integer.parseInt("1")) != null) {
                   View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                   RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                   layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                   layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                   layoutParams.setMargins(0, 0, 40, 180);
               }

               //check if gps is enabled or not and then request user to enable it
               LocationRequest locationRequest = LocationRequest.create();
               locationRequest.setInterval(10000);
               locationRequest.setFastestInterval(5000);
               locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

               LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

               SettingsClient settingsClient = LocationServices.getSettingsClient(MapActivity.this);
               Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(builder.build());

               task.addOnSuccessListener(MapActivity.this, new OnSuccessListener<LocationSettingsResponse>() {
                   @Override
                   public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                       getDeviceLocation();
                   }
               });

               task.addOnFailureListener(MapActivity.this, new OnFailureListener() {
                   @Override
                   public void onFailure(@NonNull Exception e) {
                       if (e instanceof ResolvableApiException) {
                           ResolvableApiException resolvable = (ResolvableApiException) e;
                           try {
                               resolvable.startResolutionForResult(MapActivity.this, 51);
                           } catch (IntentSender.SendIntentException e1) {
                               e1.printStackTrace();
                           }
                       }
                   }
               });

               mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                   @Override
                   public boolean onMyLocationButtonClick() {

                       if (marker!=null) {
                           marker.setVisible(false);
                       }
                       mimg.setVisibility(View.VISIBLE);
                       List<Address> addresses = null;
                       try {
                           addresses = geocoder.getFromLocation(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude(), 1);
                           latt=mLastKnownLocation.getLatitude();
                           longt=mLastKnownLocation.getLongitude();
                       } catch (IOException e) {
                           e.printStackTrace();
                       }
                       if (addresses.size() > 0) {
                           Address address = addresses.get(0);
                           String streetAddress = address.getAddressLine(0);
//                    currentMarkerLocation=LamLastKnownLocation;

                           loctext.setText(streetAddress);
                       }

                       return false;
                   }
               });
               mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                   @Override
                   public boolean onMarkerClick(Marker marker) {
                       List<Address> addresses = null;
                       try {
                           addresses = geocoder.getFromLocation(marker.getPosition().latitude, marker.getPosition().longitude, 1);
                           latt=marker.getPosition().latitude;
                           longt=marker.getPosition().longitude;
                       } catch (IOException e) {
                           e.printStackTrace();
                       }
                       if (addresses.size() > 0) {
                           Address address = addresses.get(0);
                           String streetAddress = address.getAddressLine(0);

                           marker.setTitle(streetAddress);
                           loctext.setText(streetAddress);
                       }
                       return false;
                   }
               });
               mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener(){

                   @Override
                   public void onMarkerDragStart(Marker marker) {
                       Log.d(TAG, "onMarkerDragStart: ");
                   }

                   @Override
                   public void onMarkerDrag(Marker marker) {
                       Log.d(TAG, "onMarkerDrag ");
                   }

                   @Override
                   public void onMarkerDragEnd(Marker marker) {
//                Log.d(TAG, "onMarkerDragEnd: "+streetAddress);
//                marker.getPosition().longitude;
                       List<Address> addresses = null;
                       try {
                           addresses = geocoder.getFromLocation(marker.getPosition().latitude, marker.getPosition().longitude, 1);
                           latt=marker.getPosition().latitude;
                           longt=marker.getPosition().longitude;
                       } catch (IOException e) {
                           e.printStackTrace();
                       }
                       if (addresses.size() > 0) {
                           Address address = addresses.get(0);
                           String streetAddress = address.getAddressLine(0);

//                    marker.setTitle(streetAddress);
//                    marker.setVisible(false);
                           loctext.setText(streetAddress);
                           Log.d(TAG, "onMarkerDragEnd: "+marker.getPosition().latitude+marker.getPosition().longitude);
                       }

                   }
               });

               mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                   @Override
                   public void onCameraIdle() {
                       LatLng midLatLng = mMap.getCameraPosition().target;
                       latt=midLatLng.latitude;
                       longt=midLatLng.longitude;

                       List<Address> addresses = null;
                       try {
                           addresses = geocoder.getFromLocation(midLatLng.latitude, midLatLng.longitude, 1);
//                    latt=marker.getPosition().latitude;
//                    longt=marker.getPosition().longitude;
                       } catch (IOException e) {
                           e.printStackTrace();
                       }
                       if (addresses.size() > 0) {
                           Address address = addresses.get(0);
                           String streetAddress = address.getAddressLine(0);

//                    marker.setTitle(streetAddress);
//                    marker.setVisible(false);
                           loctext.setText(streetAddress);
                           Log.d(TAG, "onMarkerDragEndidle: "+midLatLng.latitude+ midLatLng.longitude);
                       }
//                getAddress(midLatLng.latitude, midLatLng.longitude);
                   }
               });





           }
    private static final String TAG = "MapsActivity";
    private GoogleMap mMap;
    private Geocoder geocoder;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private ImageView mimg;

    protected GeoDataClient mGeoDataClient;
    private Location mLastKnownLocation;
    private LocationCallback locationCallback;
    private LatLng  currentMarkerLocation;
    private Double ltlong;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private View mapView;
    private AutoCompleteTextView loctext;
    private Marker marker;
    private Double latt,longt;

    private Button btnFind;

    private final float DEFAULT_ZOOM = 15;



           private PlaceAutocompleteAdapter mAdapter;
           private static final LatLngBounds BOUNDS_GREATER_SYDNEY = new LatLngBounds(
                   new LatLng(-34.041458, 150.790100), new LatLng(-33.682247, 151.383362));

           private AutoCompleteTextView mAutocompleteView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGeoDataClient = Places.getGeoDataClient(this, null);
        setContentView(R.layout.activity_map);
        geocoder = new Geocoder(this);

        btnFind = findViewById(R.id.btn_confirmAddr);
        loctext = findViewById(R.id.locationtext);
        loctext.setInputType(InputType.TYPE_CLASS_TEXT);
        mimg = (ImageView) findViewById(R.id.markerimg);



        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapView = mapFragment.getView();

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(MapActivity.this);

        loctext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if(actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || keyEvent.getAction() == KeyEvent.ACTION_DOWN
                        || keyEvent.getAction() == KeyEvent.KEYCODE_ENTER){

                    //execute our method for searching
                    geoLocate();
                }

                return false;
            }

            private void geoLocate(){

                Log.d(TAG, "geoLocate: geolocating");
                String searchString = loctext.getText().toString();
                Geocoder geocoder = new Geocoder(MapActivity.this);
                List<Address> list = new ArrayList<>();
                try{
                    list = geocoder.getFromLocationName(searchString, 1);
                }catch (IOException e){
                    Log.e(TAG, "geoLocate: IOException: " + e.getMessage() );
                }

                if(list.size() > 0){
                    Address address = list.get(0);

                    Log.d(TAG, "geoLocate: found a location: " + address.toString());

                    LatLng latLng=(new LatLng(address.getLatitude(), address.getLongitude()));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,DEFAULT_ZOOM));
                }
            }

            private void moveCamera(LatLng latLng, float zoom, String title){
                Log.d(TAG, "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " + latLng.longitude );
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

                if(!title.equals("My Location")){

                }

            }
        });

        btnFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LatLng midLatLng = mMap.getCameraPosition().target;
                latt=midLatLng.latitude;
                longt=midLatLng.longitude;
                Intent i = new Intent(getApplicationContext(), AddressActivity.class);
                i.putExtra("lat", String.valueOf(latt));
                i.putExtra("long", String.valueOf(longt));
                i.putExtra("cloc", String.valueOf(0));
                startActivity(i);



            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 51) {
            if (resultCode == RESULT_OK) {
                getDeviceLocation();
            }
        }
    }



    @SuppressLint("MissingPermission")
    private void getDeviceLocation() {

        if (ContextCompat.checkSelfPermission(MapActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?

            if (ActivityCompat.shouldShowRequestPermissionRationale(MapActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                new AlertDialog.Builder(this)
                        .setTitle("Required Location Permission")
                        .setMessage("You have to give this permission to acess this feature")
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(MapActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                            }
                        })
                        .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(MapActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {


        mFusedLocationProviderClient.getLastLocation()
                .addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            mLastKnownLocation = task.getResult();
                            Log.d("llocation", String.valueOf(mLastKnownLocation));
//                            mMap.addMarker(new MarkerOptions().position())
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));

                            String out = getCompleteAddressString(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                            latt=mLastKnownLocation.getLatitude();
                            longt=mLastKnownLocation.getLongitude();
                            loctext.setText("" + out);
                            if (mLastKnownLocation != null) {
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                                latt=mLastKnownLocation.getLatitude();
                                longt=mLastKnownLocation.getLongitude();
                                loctext.setText("" + out);
                            } else {
                                final LocationRequest locationRequest = LocationRequest.create();
                                locationRequest.setInterval(10000);
                                locationRequest.setFastestInterval(5000);
                                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                                locationCallback = new LocationCallback() {
                                    @Override
                                    public void onLocationResult(LocationResult locationResult) {
                                        super.onLocationResult(locationResult);
                                        if (locationResult == null) {
                                            return;
                                        }
                                        mLastKnownLocation = locationResult.getLastLocation();
                                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                                        Log.d("lastknown", mLastKnownLocation.getLatitude() + "" + mLastKnownLocation.getLongitude());
                                        String out = getCompleteAddressString(marker.getPosition().latitude, marker.getPosition().longitude);
                                        latt=marker.getPosition().latitude;
                                        longt=marker.getPosition().longitude;
                                        loctext.setText("" + out);
                                        mFusedLocationProviderClient.removeLocationUpdates(locationCallback);
                                    }
                                };
                                mFusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);

                            }
                        } else {
                            Toast.makeText(MapActivity.this, "unable to get last location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });


    }
    }


    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = address;
                Log.w("MyCurrentctionaddress", "" + strReturnedAddress.toString());
                Log.d("asd",address);
                loctext.setText(address);
            } else {
                Log.w("MyCurrentloctionaddress", "NoAddress returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("MyCurrentloctionaddress", "Canont get Address!");
        }
        return strAdd;
    }

           @Override
           public void onBackPressed() {

               finish();
               Intent intent = new Intent(MapActivity.this, AddressActivity.class);
               startActivity(intent);
           }

           public void setAppLocale(String localeCode){
               Resources res=getResources();
               DisplayMetrics dm=res.getDisplayMetrics();
               Configuration conf=res.getConfiguration();
               if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
                   conf.setLocale(new Locale(localeCode.toLowerCase()));
               }else{
                   conf.locale=new Locale(localeCode.toLowerCase());
               }
               SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
//        prefData.setCurrentLanguage(localeCode);
               editor.putString("My_Lang","ar");
               editor.apply();
               res.updateConfiguration(conf,dm);
           }


           /**
            * Listener that handles selections from suggestions from the AutoCompleteTextView that
            * displays Place suggestions.
            * Gets the place id of the selected item and issues a request to the Places Geo Data Client
            * to retrieve more details about the place.
            *
            * @see GeoDataClient#getPlaceById(String...)
            */
           private AdapterView.OnItemClickListener mAutocompleteClickListener
                   = new AdapterView.OnItemClickListener() {
               @Override
               public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
                   final AutocompletePrediction item = mAdapter.getItem(position);
                   final String placeId = item.getPlaceId();
                   final CharSequence primaryText = item.getPrimaryText(null);

                   Log.i(TAG, "Autocomplete item selected: " + primaryText);

            /*
             Issue a request to the Places Geo Data Client to retrieve a Place object with
             additional details about the place.
              */
                   Task<PlaceBufferResponse> placeResult = mGeoDataClient.getPlaceById(placeId);
                   placeResult.addOnCompleteListener(mUpdatePlaceDetailsCallback);

                   Toast.makeText(getApplicationContext(), "Clicked: " + primaryText,
                           Toast.LENGTH_SHORT).show();
                   Log.i(TAG, "Called getPlaceById to get Place details for " + placeId);
               }
           };

           /**
            * Callback for results from a Places Geo Data Client query that shows the first place result in
            * the details view on screen.
            */
           private OnCompleteListener<PlaceBufferResponse> mUpdatePlaceDetailsCallback
                   = new OnCompleteListener<PlaceBufferResponse>() {
               @Override
               public void onComplete(Task<PlaceBufferResponse> task) {
                   try {
                       PlaceBufferResponse places = task.getResult();

                       // Get the Place object from the buffer.
                       final Place place = places.get(0);
                       // Format details of the place for display and show it in a TextView.
                      /* mPlaceDetailsText.setText(formatPlaceDetails(getResources(), place.getName(),
                               place.getId(), place.getAddress(), place.getPhoneNumber(),
                               place.getWebsiteUri()));*/
                       // Display the third party attributions if set.
                       final CharSequence thirdPartyAttribution = places.getAttributions();
                    /*   if (thirdPartyAttribution == null) {
                           mPlaceDetailsAttribution.setVisibility(View.GONE);
                       } else {
                           mPlaceDetailsAttribution.setVisibility(View.VISIBLE);
                           mPlaceDetailsAttribution.setText(
                                   Html.fromHtml(thirdPartyAttribution.toString()));
                       }*/

                       Log.i(TAG, "Place details received: " + place.getName());

                       places.release();
                   } catch (RuntimeRemoteException e) {
                       // Request did not complete successfully
                       Log.e(TAG, "Place query did not complete.", e);
                       return;
                   }
               }
           };




       }
