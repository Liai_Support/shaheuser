package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lia.shahe.R;
import com.lia.shahe.utility.StaticInfo;

public class ReferalActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView hfhome,finfo,fcart,flist,fprofile,fwallet;
    private long lastClickTime = 0;
    private TextView referalcodetxt,copytxt,sharetxt;
    private String referCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referal);
        hfhome=(ImageView)findViewById(R.id.fhome);
        finfo=(ImageView)findViewById(R.id.finfo);
        fcart=(ImageView)findViewById(R.id.fcart);
        flist=(ImageView)findViewById(R.id.flistorder);
        fprofile=(ImageView)findViewById(R.id.fprofile);
        fwallet=(ImageView)findViewById(R.id.fwallet);
        referalcodetxt=findViewById(R.id.referalcode);
        copytxt=findViewById(R.id.copycode);
        sharetxt=findViewById(R.id.sharecode);

        referCode=getIntent().getStringExtra("referalCode");
        referalcodetxt.setText(referCode);


        finfo.setOnClickListener(this);
        fcart.setOnClickListener(this);
        flist.setOnClickListener(this);
        fprofile.setOnClickListener(this);
        hfhome.setOnClickListener(this);
        fwallet.setOnClickListener(this);
        copytxt.setOnClickListener(this);
        sharetxt.setOnClickListener(this);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(ReferalActivity.this,HomeScreen.class);
        startActivity(i);
    }

    @Override
    public void onClick(View view) {
        // preventing double, using threshold of 1000 ms
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
            return;
        }

        lastClickTime = SystemClock.elapsedRealtime();
        if (view == hfhome) {
            Intent intent = new Intent(this, HomeScreen.class);
            startActivity(intent);
        } else if (view == fcart) {
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);
        } else if (view == finfo) {
            Intent i = new Intent(this, AboutUs.class);
            startActivity(i);
        } else if (view == flist) {
            Intent i = new Intent(this, OrderListActivity.class);
            startActivity(i);
        } else if (view == fprofile) {
            if (StaticInfo.userId != 0) {
                Intent i = new Intent(this, ProfileActivity.class);
                startActivity(i);
            } else {
                Intent i = new Intent(this, SignInActicity.class);
                startActivity(i);
            }

        }else if (view == fwallet) {
            Intent intent = new Intent(this, WalletHistory.class);
            startActivity(intent);
        }else if (view==copytxt){
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("Copied Text", referCode);
            clipboard.setPrimaryClip(clip);
            copytxt.setText("copied");
        }else if (view==sharetxt){
            /*Create an ACTION_SEND Intent*/
            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            /*This will be the actual content you wish you share.*/
            String shareBody = referCode;
            /*The type of the content is text, obviously.*/
            intent.setType("text/plain");
            /*Applying information Subject and Body.*/
            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, referCode);
            intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            /*Fire!*/
            startActivity(Intent.createChooser(intent, referCode));
        }
    }
}