package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.shahe.R;
import com.lia.shahe.adapter.CartAdapter;
import com.lia.shahe.bo.Cart;
import com.lia.shahe.utility.Session;
import com.lia.shahe.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CartActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String Url = "listcart";
    private List<Cart> list_cart;
    private ArrayList<Cart> arraylist;
    private Session session;
    private CartAdapter cartadapter;
    private GridLayoutManager crgm;
    private RecyclerView cartrv;
    private TextView totaltxt,buynowtxt,noprod;

    public static Double cartPrice= Double.valueOf(0);
    private String delUrl="deletefromcart";
    private ProgressBar pbartxt;
    private long lastClickTime = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);
        session = new Session(this);
        this.list_cart = list_cart;

        totaltxt=(TextView)findViewById(R.id.total);
        buynowtxt=(TextView)findViewById(R.id.buynow);
        noprod=(TextView)findViewById(R.id.noprodtxt);
        pbartxt=(ProgressBar)findViewById(R.id.pbar);

        cartrv=(RecyclerView)findViewById(R.id.crecycler_view);
        crgm=new GridLayoutManager(this,1,GridLayoutManager.VERTICAL, false);
        cartrv.setLayoutManager(crgm);

        JSONObject obj=new JSONObject();
        try {
            obj.put("userId", StaticInfo.userId);
            getCartValues(String.valueOf(obj));
            Log.d("initializeuserId",""+obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        totaltxt.setText(""+StaticInfo.cartTotal);
        buynowtxt.setOnClickListener(this);
    }

    private void getCartValues(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(this);
        loadingDialog.startLoadingDialog();
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(response);

            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loadingDialog.dismissDialog();
                    try {
                        Log.d("json response",""+response);

                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error")=="true"){
                            noprod.setVisibility(View.VISIBLE);
                            buynowtxt.setVisibility(View.GONE);
                            totaltxt.setVisibility(View.GONE);
//                            Toast.makeText(getApplicationContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT).show();
                        }else if (obj.getString("error")=="false"){

//                            JSONArray dataobj=obj.getJSONArray("data");

                            list_cart=new ArrayList<>();
                            JSONArray dataArray=obj.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject carthobj = dataArray.getJSONObject(i);
                                Cart cart = new Cart(carthobj.getInt("id"),carthobj.getString("details"));
                                list_cart.add(cart);

                            }
                            setupCartData(list_cart);


                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }


            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void updatecartdelJson(String response) {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(response);
            cartPrice=0.0;

            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+delUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        Log.d("json response",""+response);

                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error").equals(true)){

                            Toast.makeText(getApplicationContext(),getString(R.string.oops),Toast.LENGTH_SHORT).show();
                        }else{

//                            JSONArray dataobj=obj.getJSONArray("data");

                            JSONObject cartobj= obj.getJSONObject("data");
                            StaticInfo.cartCount= Integer.parseInt(cartobj.getString("cartCount"));
                            Intent i=new Intent(getApplicationContext(),CartActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);




                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }


            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void setupCartData(List<Cart> list_cart) {
        cartadapter=new CartAdapter(list_cart,this,session,this);

        cartrv.setAdapter(cartadapter);
    }
    public double getTotal(ArrayList<Cart> todayCollectionlist){

        double total=5.0;
        Log.d("sdvsf", ""+total);
        for(int i=0;i<todayCollectionlist.size();i++){
//            total=total+Double.parseDouble(""+todayCollectionlist.get(i).getPayment());

        }
        Log.d("sdvsfghdfh", ""+total);
        return total;

    }


    @Override
    public void onClick(View view) {
        // preventing double, using threshold of 1000 ms
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000){
            return;
        }

        lastClickTime = SystemClock.elapsedRealtime();
        if (view==buynowtxt){
            cartPrice=0.0;
            Intent i=new Intent(getApplicationContext(),AddressList.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
    }

    @Override
    public void onBackPressed() {

        cartPrice=0.0;
        totaltxt.setText(""+cartPrice);
        finish();
        Intent intent = new Intent(CartActivity.this, HomeScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void setAppLocale(String localeCode){
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
//        prefData.setCurrentLanguage(localeCode);
        editor.putString("My_Lang","ar");
        editor.apply();
        res.updateConfiguration(conf,dm);
    }

}