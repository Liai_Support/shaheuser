package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.shahe.R;
import com.lia.shahe.adapter.CouponAdapter;
import com.lia.shahe.bo.Coupons;
import com.lia.shahe.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class CouponActivity extends AppCompatActivity {

    private List<Coupons> list_data;
    private RecyclerView couponrc;
    private GridLayoutManager cgm;
    private String Url="discountcoupons";
    private CouponAdapter cadapter;
    public String addressId;
    public Double cOrderTot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);

        addressId=getIntent().getStringExtra("addressId");
        cOrderTot=getIntent().getDoubleExtra("orderTotal",0.0);

        couponrc=(RecyclerView)findViewById(R.id.couponrc);
        cgm=new GridLayoutManager(this,1,GridLayoutManager.VERTICAL, false);
        couponrc.setLayoutManager(cgm);
        list_data=new ArrayList<>();
      /*  int largePadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing);
        int smallPadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing_small);
        couponrc.addItemDecoration(new ProductGridItemDecoration(largePadding, smallPadding));*/
        getInitialValues();

    }

    private void getInitialValues() {
        final LoadingDialog loadingDialog=new LoadingDialog(this);
        loadingDialog.startLoadingDialog();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
//            String URL = "http://...";


        final String requestcartBody = "";
        Log.d("requestjsondfdgf", String.valueOf(StaticInfo.MAIN_URL+Url));
        StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+Url,
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loadingDialog.dismissDialog();
                Log.d("strrrrsdfr", ">>" + response);
//
                try {

                    JSONObject obj = new JSONObject(response);
                    if(obj.getString("error").equals(true)){
                        Toast.makeText(getApplicationContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT);
                    }else{


                        list_data=new ArrayList<>();
                        if (!obj.isNull("data") ) {
                            JSONArray dataArray = obj.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject couponobj = dataArray.getJSONObject(i);
                                Coupons coupons = new Coupons(couponobj.getInt("id"), couponobj.getString("code"),
                                        couponobj.getString("description"), couponobj.getDouble("amount"),
                                        couponobj.getString("percentage"));
                                list_data.add(coupons);
                                setupCouponData(list_data);
                                Log.w("sd", "" + list_data.size());
                            }

                        }


                    }
//                        obj.getInt()



                } catch (JSONException e) {
                    e.printStackTrace();
                }
//


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                    return null;
                }
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);
    }

    private void setupCouponData(List<Coupons> list_data) {
        cadapter=new CouponAdapter(list_data,this,this );
        couponrc.setAdapter(cadapter);

    }
}