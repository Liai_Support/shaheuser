package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.shahe.R;
import com.lia.shahe.adapter.WalletAdapter;
import com.lia.shahe.bo.Wallet;
import com.lia.shahe.utility.Session;
import com.lia.shahe.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class WalletHistory extends AppCompatActivity implements View.OnClickListener {
    private TextView walletAmt;
    private ImageView hfhome,finfo,fcart,flist,fprofile,fwallet;
    private long lastClickTime = 0;
    private Session session;

    private List<Wallet> list_data;
    private RecyclerView walletrc;
    private GridLayoutManager wgm;
    private String Url="wallethistory";
    private  WalletAdapter wadapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_history);
        session = new Session(this);
        finfo=(ImageView)findViewById(R.id.finfo);
        hfhome=(ImageView)findViewById(R.id.fhome);
        fcart=(ImageView)findViewById(R.id.fcart);
        flist=(ImageView)findViewById(R.id.flistorder);
        fprofile=(ImageView)findViewById(R.id.fprofile);
        fwallet=(ImageView)findViewById(R.id.fwallet);
        fwallet.setImageResource(R.drawable.ic_walletselected);

        walletrc=(RecyclerView)findViewById(R.id.walletrecycle);
        wgm=new GridLayoutManager(this,1,GridLayoutManager.VERTICAL, false);
        walletrc.setLayoutManager(wgm);
        list_data=new ArrayList<>();
         int largePadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing);
        int smallPadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing_small);
        walletrc.addItemDecoration(new ProductGridItemDecoration(largePadding, smallPadding));

        walletAmt=findViewById(R.id.walletTotal);
        walletAmt.setText(""+StaticInfo.walletAmount+" SAR");
        finfo.setOnClickListener(this);
        fcart.setOnClickListener(this);
        flist.setOnClickListener(this);
        fprofile.setOnClickListener(this);
        hfhome.setOnClickListener(this);
        fwallet.setOnClickListener(this);


        JSONObject obj=new JSONObject();
        try {
            obj.put("userId", session.getUserId());
            getInitialValues(String.valueOf(obj));
            Log.d("initializeuserId",""+obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




    private void getInitialValues(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(this);
        try {
            loadingDialog.startLoadingDialog();
            RequestQueue requestQueue = Volley.newRequestQueue(this);
//            String URL = "http://...";
            JSONObject jsonBody = new JSONObject(response);

            final String requestcartBody = jsonBody.toString();
            Log.d("requestjsondfdgf", String.valueOf(StaticInfo.MAIN_URL+Url));
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loadingDialog.dismissDialog();
                    Log.d("strrrrsdfr", ">>" + response);
//
                    try {

                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error").equals(true)){
                            Toast.makeText(getApplicationContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT);
                        }else{
                            JSONObject dataobj=obj.getJSONObject("data");

//                            Log.d("asdfg", String.valueOf(dataobj.getJSONArray("banner")));


                            list_data=new ArrayList<>();
                            if (!dataobj.isNull("walletHistory") ) {
                                JSONArray walletArray = dataobj.getJSONArray("walletHistory");
                                for (int i = 0; i < walletArray.length(); i++) {
                                    JSONObject wallethistoryobj = walletArray.getJSONObject(i);
                                    Wallet wallet = new Wallet(wallethistoryobj.getInt("id"), wallethistoryobj.getString("amount"), wallethistoryobj.getString("type"), wallethistoryobj.getString("date"), wallethistoryobj.getString("remarks"));
                                    list_data.add(wallet);
                                    setupProductData(list_data);
                                    Log.w("sd", "" + list_data.size());
                                }

                            }


                        }
//                        obj.getInt()



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }


            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupProductData(List<Wallet> list_data) {
        wadapter=new WalletAdapter(list_data,this);
        walletrc.setAdapter(wadapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(WalletHistory.this,HomeScreen.class);
        startActivity(i);
    }

    @Override
    public void onClick(View view) {
        // preventing double, using threshold of 1000 ms
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
            return;
        }

        lastClickTime = SystemClock.elapsedRealtime();
        if (view == hfhome) {
            Intent intent = new Intent(this, HomeScreen.class);
            startActivity(intent);
        } else if (view == fcart) {
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);
        } else if (view == finfo) {
            Intent i = new Intent(this, AboutUs.class);
            startActivity(i);
        } else if (view == flist) {
            Intent i = new Intent(this, OrderListActivity.class);
            startActivity(i);
        } else if (view == fprofile) {
            if (StaticInfo.userId != 0) {
                Intent i = new Intent(this, ProfileActivity.class);
                startActivity(i);
            } else {
                Intent i = new Intent(this, SignInActicity.class);
                startActivity(i);
            }

        }else if (view == fwallet) {
            Intent intent = new Intent(this, WalletHistory.class);
            startActivity(intent);
        }

    }
}