package com.lia.shahe.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.lia.shahe.R;

import java.util.concurrent.TimeUnit;

public class VerifyPhoneActivity extends AppCompatActivity implements View.OnClickListener {


    private String verificationId;
    private FirebaseAuth mAuth;
    private ProgressBar progressBar;
    private EditText editText,editTextCode1txt,editTextCode2txt,editTextCode3txt,editTextCode4txt,editTextCode5txt,editTextCode6txt;
    private String phonenumber,username,email,mobileStr;
    private View contextView;
    private TextView expires,resendotp,textViewmobtxt;
    private Button signin;
    String otp,code="";
    PhoneAuthProvider.ForceResendingToken mResendToken;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);

       // mAuth = FirebaseAuth.getInstance();
        Log.d("mauth",""+mAuth);


//        editText = findViewById(R.id.editTextCode);
        editTextCode1txt = findViewById(R.id.editTextCode1);
        editTextCode2txt = findViewById(R.id.editTextCode2);
        editTextCode3txt = findViewById(R.id.editTextCode3);
        editTextCode4txt = findViewById(R.id.editTextCode4);
      //  editTextCode5txt = findViewById(R.id.editTextCode5);
      //  editTextCode6txt = findViewById(R.id.editTextCode6);
        textViewmobtxt = findViewById(R.id.textViewmob);
        expires=findViewById(R.id.expiresin);
        resendotp=findViewById(R.id.resendcode);
        signin=findViewById(R.id.buttonSignIn);

        mobileStr = getIntent().getStringExtra("phoneNo");
        otp = getIntent().getStringExtra("otp");
         phonenumber="+966"+mobileStr;
        textViewmobtxt.setText("+966"+mobileStr);

        Log.d("phonenumber",""+phonenumber);
       // sendVerificationCode(phonenumber);

        resendotp.setOnClickListener(this);
        signin.setOnClickListener(this);

        EditText[] edit = {editTextCode1txt, editTextCode2txt, editTextCode3txt, editTextCode4txt};

        editTextCode1txt.addTextChangedListener(new GenericTextWatcher(editTextCode1txt, edit));
        editTextCode2txt.addTextChangedListener(new GenericTextWatcher(editTextCode2txt, edit));
        editTextCode3txt.addTextChangedListener(new GenericTextWatcher(editTextCode3txt, edit));
        editTextCode4txt.addTextChangedListener(new GenericTextWatcher(editTextCode4txt, edit));



        editTextCode1txt.requestFocus();
     /*   View.OnKeyListener key=new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(!((EditText) v).toString().isEmpty() && ((EditText)v).toString().length()>1)

                    v.focusSearch(View.FOCUS_RIGHT).requestFocus();
                //v.requestFocus();
                return false;
            }
        };

      //  editTextCode6txt.setOnKeyListener(key);
       // editTextCode5txt.setOnKeyListener(key);
        editTextCode4txt.setOnKeyListener(key);
        editTextCode3txt.setOnKeyListener(key);
        editTextCode2txt.setOnKeyListener(key);
        editTextCode1txt.setOnKeyListener(key);
*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!code.equals("")){
            Toast.makeText(VerifyPhoneActivity.this,otp,Toast.LENGTH_SHORT).show();
        }
    }

    private void verifyCode(String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithCredential(credential);
    }

    private void signInWithCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            if (getIntent().getStringExtra("action").equals("signup")) {
                                Intent intent = new Intent(VerifyPhoneActivity.this, SignUpActivity.class);
                                intent.putExtra("phoneNo", mobileStr);

                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                                startActivity(intent);
                            }else{
                                Intent intent = new Intent(VerifyPhoneActivity.this, PasswordActivity.class);
                                intent.putExtra("phoneNo", mobileStr);

                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                                startActivity(intent);
                            }

                        } else {
//                            snackbarToast("Verification Failed");
//                            Toast.makeText(VerifyPhoneActivity.this,"Verification Failed",Toast.LENGTH_LONG).show();

                        }
                    }
                });
    }

    private void sendVerificationCode(String number) {


        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,        // Phone number to verify
                30,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallBack);        // OnVerificationStateChangedCallbacks

    }
    public void reverseTimer(int Seconds){

        new CountDownTimer(Seconds* 1000+1000, 1000) {

            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);
                int minutes = seconds / 60;
                seconds = seconds % 60;
                expires.setText("Expires in : " + String.format("%02d", minutes)
                        + ":" + String.format("%02d", seconds));
                resendotp.setVisibility(View.GONE);
            }

            public void onFinish() {
                expires.setText("Expired");
                resendotp.setVisibility(View.VISIBLE);
            }
        }.start();
    }



    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            Log.d("TAG", "onCodeSent:" + s);
            super.onCodeSent(s, forceResendingToken);
            //reverseTimer(30);
            // Save verification ID and resending token so we can use them later
            verificationId = s;
            mResendToken = forceResendingToken;
            Log.d("TAG", "onCodeSentsds:" + forceResendingToken);
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();

            if (code != null) {

                for (int i=0;i>code.length();i++){
                    if (i==1) {
                        editTextCode6txt.setText(code);
                    }else if (i==2){
                        editTextCode5txt.setText(code);
                    }else if (i==3){
                        editTextCode4txt.setText(code);
                    }else if (i==4){
                        editTextCode3txt.setText(code);
                    }else if (i==5){
                        editTextCode2txt.setText(code);
                    }else if (i==6){
                        editTextCode1txt.setText(code);
                    }
                }

//                editText.getEditText().setText(code);
                verifyCode(code);
                Log.d("TAG", "onCodecomplete:" + code);

            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Log.w("Failed TAG", "onVerificationFailed", e);
            Log.w("", "onVerificationFailed", e);
            if (e instanceof FirebaseAuthInvalidCredentialsException) {

                Toast.makeText(VerifyPhoneActivity.this, getString(R.string.invalidphone), Toast.LENGTH_LONG).show();

            } else if (e instanceof FirebaseTooManyRequestsException) {
                // error log
                Toast.makeText(VerifyPhoneActivity.this, getString(R.string.verificationfailed), Toast.LENGTH_LONG).show();

            }else{
                Toast.makeText(VerifyPhoneActivity.this, getString(R.string.verificationfailed), Toast.LENGTH_LONG).show();

            }


        }
    };

    @Override
    public void onClick(View view) {
        if (view==signin){

            String code1 = editTextCode1txt.getText().toString().trim();
            String code2 = editTextCode2txt.getText().toString().trim();
            String code3 = editTextCode3txt.getText().toString().trim();
            String code4 = editTextCode4txt.getText().toString().trim();
//            String code5 = editTextCode5txt.getText().toString().trim();
 //           String code6 = editTextCode6txt.getText().toString().trim();
            code=code1+code2+code3+code4;

            if (code.length()==4){
                Log.d("combined otp1",code);
                Log.d("combined otp2",otp);
              //  verifyCode(code);
                if(otp.equalsIgnoreCase(code)){
                    if (getIntent().getStringExtra("action").equals("signup")) {
                        Intent intent = new Intent(VerifyPhoneActivity.this, SignUpActivity.class);
                        intent.putExtra("phoneNo", mobileStr);

                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        startActivity(intent);
                    }else{
                        Intent intent = new Intent(VerifyPhoneActivity.this, PasswordActivity.class);
                        intent.putExtra("phoneNo", mobileStr);

                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        startActivity(intent);
                    }
                }
                else {
                    Toast.makeText(VerifyPhoneActivity.this,"Enter Valid OTP",Toast.LENGTH_LONG).show();
                }
            }else{
                Toast.makeText(VerifyPhoneActivity.this,"Enter Valid OTP",Toast.LENGTH_LONG).show();
            }


        }
        else if (view==resendotp){

            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    phonenumber,        // Phone number to verify
                    30  ,               // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    this,               // Activity (for callback binding)
                    mCallBack,         // OnVerificationStateChangedCallbacks
                    mResendToken);             // Force Resending Token from callbacks

        }
    }

}