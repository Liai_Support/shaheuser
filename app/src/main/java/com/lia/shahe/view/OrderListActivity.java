package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.shahe.R;
import com.lia.shahe.adapter.OrderListAdapter;
import com.lia.shahe.bo.Order;
import com.lia.shahe.utility.Session;
import com.lia.shahe.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class OrderListActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String Url = "listorder";
    private List<Order> list_order;

    private Session session;
    private OrderListAdapter orderAdapter;
    private GridLayoutManager crgm;
    private RecyclerView cartrv;
    private TextView totaltxt,buynowtxt,noprod;
    public static Double cartPrice= Double.valueOf(0);
    private ImageView hfhome,finfo,fcart,flist,fprofile,fwallet;


    private long lastClickTime = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);
        session = new Session(this);
        this.list_order = list_order;
        noprod=(TextView)findViewById(R.id.noprodtxt);
        hfhome=(ImageView)findViewById(R.id.fhome);
        finfo=(ImageView)findViewById(R.id.finfo);
        fcart=(ImageView)findViewById(R.id.fcart);
        flist=(ImageView)findViewById(R.id.flistorder);
        fprofile=(ImageView)findViewById(R.id.fprofile);
        fwallet=(ImageView)findViewById(R.id.fwallet);
        flist.setImageResource(R.drawable.ic_list_selected);

        cartrv=(RecyclerView)findViewById(R.id.crecycler_view);
        crgm=new GridLayoutManager(this,1,GridLayoutManager.VERTICAL, false);
        cartrv.setLayoutManager(crgm);

        JSONObject obj=new JSONObject();
        try {
            obj.put("userId", StaticInfo.userId);
            getOrderValues(String.valueOf(obj));
            Log.d("initializeuserId",""+obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        hfhome.setOnClickListener(this);
        finfo.setOnClickListener(this);
        fcart.setOnClickListener(this);
        flist.setOnClickListener(this);
        fprofile.setOnClickListener(this);
        fwallet.setOnClickListener(this);
    }

    private void getOrderValues(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(this);
        loadingDialog.startLoadingDialog();
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(response);

            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loadingDialog.dismissDialog();
                    try {
                        Log.d("json response",""+response);

                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error")=="true"){
                            noprod.setVisibility(View.VISIBLE);
//                            buynowtxt.setVisibility(View.GONE);
//                            totaltxt.setVisibility(View.GONE);
//                            Toast.makeText(getApplicationContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT).show();
                        }else if (obj.getString("error")=="false"){

//                            JSONArray dataobj=obj.getJSONArray("data");
//                            (int id,int apporderId,Double orderTotal,String paymentType,String orderDate,String orderStatus,
//                                    String details,String addressDetails)
                            list_order=new ArrayList<>();
                            JSONArray dataArray=obj.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject orderobj = dataArray.getJSONObject(i);
                                Order order = new Order(orderobj.getInt("orderid"),orderobj.getInt("applicationOrderId"),
                                        orderobj.getDouble("orderTotal"),orderobj.getInt("vat"),orderobj.getString("paymentType"),orderobj.getString("orderDate"),
                                        orderobj.getString("orderStatus"),orderobj.getString("orderdetails"),orderobj.getString("addressdetails"));
                                list_order.add(order);

                            }
                            setupCartData(list_order);


                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }


            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void setupCartData(List<Order> list_order) {
        orderAdapter=new OrderListAdapter(list_order,this,session,this);

        cartrv.setAdapter(orderAdapter);
    }


    @Override
    public void onClick(View view) {
        // preventing double, using threshold of 1000 ms
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
            return;
        }

        lastClickTime = SystemClock.elapsedRealtime();
        if (view == hfhome) {
            Intent intent = new Intent(this, HomeScreen.class);
            startActivity(intent);
        } else if (view == fcart) {
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);
        } else if (view == finfo) {
            Intent i = new Intent(this, AboutUs.class);
            startActivity(i);
        } else if (view == flist) {
            Intent i = new Intent(this, OrderListActivity.class);
            startActivity(i);
        } else if (view == fwallet) {
            Intent i = new Intent(this, WalletHistory.class);
            startActivity(i);
        } else if (view == fprofile) {
            if (StaticInfo.userId != 0) {
                Intent i = new Intent(this, ProfileActivity.class);
                startActivity(i);
            } else {
                Intent i = new Intent(this, SignInActicity.class);
                startActivity(i);
            }

        }
    }

    @Override
    public void onBackPressed() {

        finish();
        Intent intent = new Intent(OrderListActivity.this, HomeScreen.class);
        startActivity(intent);
    }
    public void setAppLocale(String localeCode){
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
//        prefData.setCurrentLanguage(localeCode);
        editor.putString("My_Lang","ar");
        editor.apply();
        res.updateConfiguration(conf,dm);
    }

}