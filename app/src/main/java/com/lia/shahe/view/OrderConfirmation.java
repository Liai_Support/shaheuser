package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lia.shahe.R;
import com.lia.shahe.utility.StaticInfo;

import java.util.Locale;

public class OrderConfirmation extends AppCompatActivity implements View.OnClickListener{
    private TextView orderidtxt;
    private ImageView hfhome,finfo,fcart,flist,fprofile,fwallet;
    private long lastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_confirmation);

        orderidtxt=(TextView)findViewById(R.id.orderid);
        orderidtxt.setText(getIntent().getStringExtra("orderId"));
        hfhome=(ImageView)findViewById(R.id.fhome);
        finfo=(ImageView)findViewById(R.id.finfo);
        fcart=(ImageView)findViewById(R.id.fcart);
        flist=(ImageView)findViewById(R.id.flistorder);
        fprofile=(ImageView)findViewById(R.id.fprofile);
        fwallet=(ImageView)findViewById(R.id.fwallet);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                StaticInfo.cartCount=0;
                Intent i = new Intent(getApplicationContext(), HomeScreen.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        }, 3000);
        hfhome.setOnClickListener(this);
        finfo.setOnClickListener(this);
        fcart.setOnClickListener(this);
        flist.setOnClickListener(this);
        fprofile.setOnClickListener(this);
        fwallet.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        // preventing double, using threshold of 1000 ms
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000){
            return;
        }

        lastClickTime = SystemClock.elapsedRealtime();



         if (view == hfhome) {
             StaticInfo.cartCount=0;
            Intent intent = new Intent(this, HomeScreen.class);
             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }

        else if (view==fcart){
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);
        }else if (view==finfo){
            Intent i=new Intent(this,AboutUs.class);
            startActivity(i);
        }else if (view==flist){
            Intent i=new Intent(this,OrderListActivity.class);
            startActivity(i);
        }else if (view==fwallet){
            Intent i=new Intent(this,WalletHistory.class);
            startActivity(i);
        }
        else if (view==fprofile){
            if (StaticInfo.userId!=0) {
                Intent i = new Intent(this, ProfileActivity.class);
                startActivity(i);
            }else {
                Intent i = new Intent(this, SignInActicity.class);
                startActivity(i);
            }
        }
    }
    @Override
    public void onBackPressed() {
//        subtottxt.setText(""+0);
//        cartPrice=0.0;
        StaticInfo.cartCount=0;
        finish();
        Intent intent = new Intent(OrderConfirmation.this, AddressList.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void setAppLocale(String localeCode){
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
//        prefData.setCurrentLanguage(localeCode);
        editor.putString("My_Lang","ar");
        editor.apply();
        res.updateConfiguration(conf,dm);
    }

}