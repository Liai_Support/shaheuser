package com.lia.shahe.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.lia.shahe.R;
import com.lia.shahe.utility.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;
import static com.lia.shahe.view.CartActivity.cartPrice;

public class AddressActivity extends AppCompatActivity implements
        ConnectionCallbacks,
        OnConnectionFailedListener,
        LocationListener, View.OnClickListener {

    //Define a request code to send to Google Play services
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude;
    private double currentLongitude;
    private EditText loctxt,phno,flattxt,buildtxt;
    private TextView saveaddrtxt;
    private Geocoder geocoder;
    private String Url="addaddress";
    private ImageView hfhome,finfo,fcart,flist,fprofile,mapbtn,fwallet;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        cartPrice=0.0;


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);
        loctxt=(EditText) findViewById(R.id.address);
        phno=(EditText) findViewById(R.id.phone_no);
        flattxt=(EditText) findViewById(R.id.flat);
        buildtxt=(EditText) findViewById(R.id.villa);
        saveaddrtxt=(TextView)findViewById(R.id.saveaddrs);

        mapbtn=(ImageView)findViewById(R.id.map);
        hfhome=(ImageView)findViewById(R.id.fhome);
        finfo=(ImageView)findViewById(R.id.finfo);
        fcart=(ImageView)findViewById(R.id.fcart);
        flist=(ImageView)findViewById(R.id.flistorder);
        fprofile=(ImageView)findViewById(R.id.fprofile);
        fwallet=(ImageView)findViewById(R.id.fwallet);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        saveaddrtxt.setOnClickListener(this);
        hfhome.setOnClickListener(this);
        finfo.setOnClickListener(this);
        fcart.setOnClickListener(this);
        flist.setOnClickListener(this);
        fprofile.setOnClickListener(this);
        fwallet.setOnClickListener(this);
        mapbtn.setOnClickListener(this);
        if( getIntent().getExtras() != null)
        {
            //do here
            String lat= getIntent().getStringExtra("lat");
            String longt= getIntent().getStringExtra("long");
            String add= getCompleteAddressString(Double.parseDouble(lat),Double.parseDouble(longt));
//            phno.setText(add);
//            Log.d("adfasdfsadfsdfsd",getIntent().getStringExtra("cloc"));
//            loctxt.setText(add);
//            Toast.makeText(getApplicationContext(),lat+","+longt,Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        //Now lets connect to the API
        if( getIntent().getExtras() == null) {
            mGoogleApiClient.connect();
        }else if(getIntent().getStringExtra("cloc")=="1"){
            //do here
            Log.d("sdfg","sdfghjkldfghj");
            Log.d("sdfg",getIntent().getStringExtra("cloc"));
            mGoogleApiClient.connect();
        }else if(getIntent().getStringExtra("cloc").equals(0)){
            Log.d("sdfgadasdasda",getIntent().getStringExtra("cloc"));
            String lat= getIntent().getStringExtra("lat");
            String longt= getIntent().getStringExtra("long");
            String add= getCompleteAddressString(Double.parseDouble(lat),Double.parseDouble(longt));
            loctxt.setText(add);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v(this.getClass().getSimpleName(), "onPause()");

        //Disconnect from API onPause()
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }


    }

    /**
     * If connected get lat and long
     *
     */
    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        } else {
            //If everything went fine lets get latitude and longitude
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            Log.d("sdfgh",currentLatitude + " WORKS " + currentLongitude + "");
            getCompleteAddressString(currentLatitude,currentLongitude);
            loctxt.setText(getCompleteAddressString(currentLatitude,currentLongitude));

//            Toast.makeText(this, currentLatitude + " WORKS " + currentLongitude + "", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    /**
     * If locationChanges change lat and long
     *
     *
     * @param location
     */


    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        Log.d("sdfghd",currentLatitude + " WORKS " + currentLongitude + "");
        getCompleteAddressString(currentLatitude,currentLongitude);
//        loctxt.setText(currentLatitude + " WORKS " + currentLongitude + "");

        loctxt.setText(getCompleteAddressString(currentLatitude,currentLongitude));


//        Toast.makeText(this, currentLatitude + " WORKS " + currentLongitude + "", Toast.LENGTH_LONG).show();
    }

    @SuppressLint("LongLogTag")
    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                loctxt.setText(strAdd);
                Log.w("My Current loction address", strReturnedAddress.toString());
            } else {
                Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }

    @Override
    public void onClick(View view) {
        if (view == saveaddrtxt) {
            if (loctxt.getText().toString().equals("") ){
                Toast.makeText(this, getString(R.string.turnonlocation), Toast.LENGTH_LONG).show();

            }

            else if ( phno.getText().toString().equals("") || flattxt.getText().toString().equals("")
                    || buildtxt.getText().toString().equals("")) {
                Toast.makeText(this, getString(R.string.enterallfield), Toast.LENGTH_LONG).show();
            } else {
//                {"userId":"2","latitude":"2","longitude":"1","address":"1","buildingname":"address","flatno":"1","phone":"1"}


                try {
                    JSONObject reqobj = new JSONObject();
                    reqobj.put("userId", StaticInfo.userId);
                    reqobj.put("latitude", currentLatitude);
                    reqobj.put("longitude", currentLongitude);
                    reqobj.put("address", loctxt.getText().toString());
                    reqobj.put("buildingname", buildtxt.getText().toString());
                    reqobj.put("flatno", flattxt.getText().toString());
                    reqobj.put("phone", phno.getText().toString());
                    getaddressrequest(reqobj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        } else if (view == hfhome) {
            Intent intent = new Intent(this, HomeScreen.class);
            startActivity(intent);
        } else if (view == fcart) {
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);
        } else if (view == finfo) {
            Intent i = new Intent(this, AddressActivity.class);
            startActivity(i);
        } else if (view == flist) {
            Intent i = new Intent(this, OrderHistory.class);
            startActivity(i);
        }else if (view == fwallet) {
            Intent i = new Intent(this, WalletHistory.class);
            startActivity(i);
        } else if (view == fprofile) {
            if (StaticInfo.userId != 0) {
                Intent i = new Intent(this, ProfileActivity.class);
                startActivity(i);
            } else {
                Intent i = new Intent(this, SignInActicity.class);
                startActivity(i);
            }
        }
        else if (view == mapbtn) {
            Intent i = new Intent(this, MapActivity.class);
            startActivity(i);
        }
    }

    private void getaddressrequest(JSONObject response) {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(String.valueOf(response));
            Log.d("request",String.valueOf(response));
            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {

                        JSONObject obj = new JSONObject(response);
                        Log.d("response",obj.getString("error"));
//                        Intent i=new Intent(AddressActivity.this,AddressList.class);
//                        startActivity(i);
                        if(obj.getString("error")=="true"){
                            Toast.makeText(getBaseContext(),getString(R.string.oops),Toast.LENGTH_SHORT).show();

                        }else if (obj.getString("error")=="false"){

                            Intent i=new Intent(AddressActivity.this,AddressList.class);
                            startActivity(i);
//                            Toast.makeText(getBaseContext(),"Address added to cart",Toast.LENGTH_SHORT).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }


            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onBackPressed() {
        cartPrice=0.0;
        finish();
        Intent intent = new Intent(AddressActivity.this, AddressList.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
    public void setAppLocale(String localeCode){
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
//        prefData.setCurrentLanguage(localeCode);
        editor.putString("My_Lang","ar");
        editor.apply();
        res.updateConfiguration(conf,dm);
    }

}