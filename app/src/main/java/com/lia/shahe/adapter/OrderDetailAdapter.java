package com.lia.shahe.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.lia.shahe.view.OrderDetails;
import com.lia.shahe.R;
import com.lia.shahe.bo.OrderDetailsBo;
import com.lia.shahe.utility.Session;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static java.lang.Float.parseFloat;

public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.ViewHolder> {
    List<OrderDetailsBo> list_orderbo;

    Context ct;
    private Session session;
    private OrderDetails orderDetailsActivity;

    private long lastClickTime = 0;



    public OrderDetailAdapter(List<OrderDetailsBo> list_orderbo, Context ct, Session session, OrderDetails orderDetailsActivity) {
        this.list_orderbo = list_orderbo;
        this.ct = ct;
        this.session=session;
        this.orderDetailsActivity=orderDetailsActivity;
    }

    @Override
    public OrderDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.order_details_layout,parent,false);
        return new OrderDetailAdapter.ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(final OrderDetailAdapter.ViewHolder holder, int position) {
        final OrderDetailsBo listOrderDetData=list_orderbo.get(position);

//        final Cart listCartData=list_cart.get(position);
        Log.d("araay",""+listOrderDetData.getDetails());

        try {
            JSONObject ordobj=new JSONObject(listOrderDetData.getDetails());

            holder.productname.setText(""+ordobj.getString("productname"));
            Picasso.get()
                    .load(ordobj.getString("pimage"))
                    .into(holder.pImg);
            holder.qtytxt.setText(""+ordobj.getString("qty"));
            holder.branchtxt.setText(""+ordobj.getString("branchName"));

            Double caprice= Double.valueOf(0);
//            final TextView totCartprice=(TextView)((Activity)cartActivity).findViewById(R.id.total);

                final JSONObject pdetailsobj=new JSONObject(ordobj.getString("productDetails"));
                holder.size.setText(pdetailsobj.getString("desc")+"("+pdetailsobj.getString("price")+")");
                JSONObject cstyledetailsobj=new JSONObject(ordobj.getString("cuttingStyle"));
                holder.cstyletxt.setText(cstyledetailsobj.getString("desc"));
                JSONObject pstyledetailsobj=new JSONObject(ordobj.getString("packingStyle"));
                holder.pstyletxt.setText(pstyledetailsobj.getString("desc"));
                final JSONObject kheemadetailsobj=new JSONObject(ordobj.getString("kheemaDetails"));
                holder.Kheematxt.setText(kheemadetailsobj.getString("desc")+"("+kheemadetailsobj.getString("price")+")");
                final Double cprice=Double.parseDouble(ordobj.getString("qty"))*Double.parseDouble(pdetailsobj.getString("price"));
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    double totprice=Double.sum(cprice, parseFloat(""+kheemadetailsobj.getString("price")));
                    holder.cpricetxt.setText(""+totprice+"SAR");
                    caprice=caprice+totprice;
                    Log.d("asd",""+caprice);
                }else {
                    double totprice=cprice+Float.parseFloat(""+kheemadetailsobj.getString("price"));
                    holder.cpricetxt.setText(""+totprice+"SAR");
                    caprice=caprice+totprice;

                }





            caprice=caprice;

            Log.d("asd outer1",""+caprice);
//            totCartprice.setText(""+ cartPrice);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }





    @Override
    public int getItemCount() {
        return list_orderbo.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView img,pImg;
        private TextView txt,productname,size,cstyletxt,qtytxt,Kheematxt,pstyletxt,branchtxt,cpricetxt,plusbtntxt,minusbtntxt,removetxt,qtytext;
        private LinearLayout blinear;

        public ViewHolder(View itemView) {
            super(itemView);
            pImg=(ImageView)itemView.findViewById(R.id.pimg);
            productname=(TextView)itemView.findViewById(R.id.prod_name);
            size=(TextView)itemView.findViewById(R.id.psize);
            cstyletxt=(TextView)itemView.findViewById(R.id.cstyle);
            qtytxt=(TextView)itemView.findViewById(R.id.qty);
//            qtytext=(TextView)itemView.findViewById(R.id.qtytext);
            Kheematxt=(TextView)itemView.findViewById(R.id.Kheema);
            pstyletxt=(TextView)itemView.findViewById(R.id.pstyle);
            cpricetxt=(TextView)itemView.findViewById(R.id.cprice);
            branchtxt=(TextView)itemView.findViewById(R.id.branch);
            blinear=(LinearLayout)itemView.findViewById(R.id.odlinear);
        }
    }
}