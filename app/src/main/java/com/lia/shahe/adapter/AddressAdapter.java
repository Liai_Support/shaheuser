package com.lia.shahe.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.shahe.view.AddressList;
import com.lia.shahe.R;
import com.lia.shahe.bo.Address;
import com.lia.shahe.utility.Session;
import com.lia.shahe.utility.StaticInfo;
import com.lia.shahe.view.OrderPreview;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.List;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {
    List<Address> list_address;
    Context ct;
    private Session session;
    private String Url="deleteaddress";

    public AddressAdapter(List<Address> list_address, Context ct, Session session) {
        this.list_address = list_address;
        this.ct = ct;
        this.session = session;
    }

    @Override
    public AddressAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.address_layout, parent, false);
        return new AddressAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(AddressAdapter.ViewHolder holder, int position) {
        final Address listAddressData = list_address.get(position);

        holder.txt.setText(listAddressData.getAddresstxt());
        holder.phtxt.setText(listAddressData.getPhone());
        holder.blinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("adressid",""+listAddressData.getId());
                Intent intent = new Intent(ct, OrderPreview.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("addressId",String.valueOf(listAddressData.getId()));
                intent.putExtra("couponId","0");
                ct.startActivity(intent);
            }
        });
        holder.del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject obj=new JSONObject();
                try {
                    obj.put("addressid", listAddressData.getId());
                    getDelValues(String.valueOf(obj));
                    Log.d("initializeuserId",""+obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            private void getDelValues(String response) {
                try {
                    RequestQueue requestQueue = Volley.newRequestQueue(ct);
                    JSONObject jsonBody = new JSONObject(response);

                    final String requestcartBody = jsonBody.toString();
                    StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+Url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {

                                JSONObject obj = new JSONObject(response);
                                if(obj.getString("error")=="true"){
                                    Toast.makeText(ct,"Oops Error while fetch data",Toast.LENGTH_SHORT).show();
                                }else{

//                            JSONArray dataobj=obj.getJSONArray("data");
                                    Intent intent = new Intent(ct, AddressList.class);
                                    ct.startActivity(intent);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("VOLLEY", error.toString());
                        }
                    }) {
                        @Override
                        public String getBodyContentType() {
                            return "application/json; charset=utf-8";
                        }

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                                return null;
                            }
                        }


                    };

                    requestQueue.add(stringRequest);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return list_address.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txt,phtxt,del;
        private LinearLayout blinear;

        public ViewHolder(View itemView) {
            super(itemView);

            txt = (TextView) itemView.findViewById(R.id.addrtxt);
            phtxt = (TextView) itemView.findViewById(R.id.flattxt);
            blinear = (LinearLayout) itemView.findViewById(R.id.linear);
            del = (TextView) itemView.findViewById(R.id.delimg);
        }
    }
}