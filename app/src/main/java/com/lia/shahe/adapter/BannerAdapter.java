package com.lia.shahe.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.lia.shahe.R;
import com.lia.shahe.bo.Banner;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BannerAdapter  extends RecyclerView.Adapter<BannerAdapter.ViewHolder> {
    List<Banner> list_banner;
    Context ct;

    public BannerAdapter(List<Banner> list_banner, Context ct) {
        this.list_banner = list_banner;
        this.ct = ct;
    }

    @Override
    public BannerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.banner_layout, parent, false);
        return new BannerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BannerAdapter.ViewHolder holder, int position) {
        final Banner listBanner = list_banner.get(position);
        Picasso.get()
                .load(listBanner.getBimage())
                .into(holder.img);
        Log.d("image", listBanner.getBimage());


    }

    @Override
    public int getItemCount() {
        return list_banner.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;

        public ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.product_image);
        }
    }
}
