package com.lia.shahe.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.lia.shahe.R;
import com.lia.shahe.bo.Cart;
import com.lia.shahe.utility.Session;
import com.lia.shahe.view.OrderPreview;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

//import static com.lia.shahe.view.CartActivity.cartPrice;
import static java.lang.Float.parseFloat;

public class OrderPreviewAdapter extends RecyclerView.Adapter<OrderPreviewAdapter.ViewHolder> {
    List<Cart> list_cart;

    Context ct;
    private Session session;
    private OrderPreview orderPreviewActivity;
    private String Url="updatecart";
    private  Double cartPrice=0.0;



    public OrderPreviewAdapter(List<Cart> list_cart, Context ct,Session session,OrderPreview orderPreviewActivity) {
        this.list_cart = list_cart;
        this.ct = ct;
        this.session=session;
        this.orderPreviewActivity=orderPreviewActivity;
    }

    @Override
    public OrderPreviewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.order_preview_layout,parent,false);
        return new OrderPreviewAdapter.ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(final OrderPreviewAdapter.ViewHolder holder, int position) {
        final Cart listCartData=list_cart.get(position);
        final TextView subtotprice=(TextView)((Activity)orderPreviewActivity).findViewById(R.id.subtotval);
        final TextView totvattotal=(TextView)((Activity)orderPreviewActivity).findViewById(R.id.vatperval);
        final TextView ordtotprice=(TextView)((Activity)orderPreviewActivity).findViewById(R.id.ordtotval);

        totvattotal.setText(""+session.getavat());
        Log.d("sesvat",session.getavat());
        String vattot= totvattotal.getText().toString().trim();
        try {
            JSONArray cartArray=new JSONArray(listCartData.getDetails());

            Double caprice= Double.valueOf(0);

            for (int i = 0; i < cartArray.length(); i++) {
                final JSONObject cartobj = cartArray.getJSONObject(i);
                cartobj.getString("userId");
                Log.d("sdfgfghj",""+cartobj.getString("userId"));


                holder.qtytxt.setText(""+cartobj.getString("qty"));

                final JSONObject pdetailsobj=new JSONObject(cartobj.getString("productDetails"));
                holder.productname.setText(pdetailsobj.getString("desc"));
                JSONObject cstyledetailsobj=new JSONObject(cartobj.getString("cuttingStyle"));

                JSONObject pstyledetailsobj=new JSONObject(cartobj.getString("packingStyle"));

                final JSONObject kheemadetailsobj=new JSONObject(cartobj.getString("kheemaDetails"));
                holder.proddetail.setText(""+pdetailsobj.getString("price")+" مع "+cstyledetailsobj.getString("desc")+
                        "  داخل التقط  "+pstyledetailsobj.getString("desc")+" التعبئة ");
//                holder.Kheematxt.setText(kheemadetailsobj.getString("desc")+"("+kheemadetailsobj.getString("price")+")");
                final Double cprice=Double.parseDouble(cartobj.getString("qty"))*Double.parseDouble(pdetailsobj.getString("price"));
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    double totprice=Double.sum(cprice, parseFloat(""+kheemadetailsobj.getString("price")));
                    holder.opricetext.setText(""+totprice);
                    caprice=caprice+totprice;
                    Log.d("asd",""+caprice);
                }else {
                    double totprice=cprice+Float.parseFloat(""+kheemadetailsobj.getString("price"));
                    holder.opricetext.setText(""+totprice);
                    caprice=caprice+totprice;
                    Log.d("asd"+cartArray.getJSONObject(i),""+caprice);
                }




            }
            cartPrice= cartPrice+caprice;
            caprice=caprice;

            Log.d("asd outer1",""+caprice);
            subtotprice.setText(""+ cartPrice+" SAR");
            totvattotal.setText(" % "+ session.getavat());
            Log.d("totordprev",""+cartPrice+"/"+ session.getavat());
            Log.d("totordprevj",""+cartPrice+"/"+ session.getavat()+"="+(cartPrice / 100.0f) );
            double res = (cartPrice / 100.0f) * Double.parseDouble(session.getavat());
            Double ototPrice=cartPrice+res;
            ototPrice=ototPrice-orderPreviewActivity.sCorderTot;
            ordtotprice.setText(""+ototPrice+" SAR");
            orderPreviewActivity.orderTotal=ototPrice;

//            ordtotprice.setText(""+  Math.round ((cartPrice /StaticInfo.vatPrice) * 100));
            Log.d("sdfghj",""+cartArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }



    }





    @Override
    public int getItemCount() {
        return list_cart.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView img,pimg;
        private TextView proddetail,productname,opricetext,qtytxt,branchtxt;

        private LinearLayout blinear;

        public ViewHolder(View itemView) {
            super(itemView);

            qtytxt=(TextView)itemView.findViewById(R.id.qtyval);
            productname=(TextView)itemView.findViewById(R.id.orprevprodval);
            proddetail=(TextView)itemView.findViewById(R.id.orprevprodetval);
            opricetext=(TextView)itemView.findViewById(R.id.ordprepriceval);
            branchtxt=(TextView)itemView.findViewById(R.id.branch);


        }
    }
}