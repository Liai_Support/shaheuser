package com.lia.shahe.adapter;

import android.content.Context;
import android.content.Intent;
//import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.lia.shahe.view.ProductDetails;
import com.lia.shahe.R;
import com.lia.shahe.bo.Product;
import com.lia.shahe.utility.StaticInfo;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    List<Product> list_data;
    Context ct;

    public ProductAdapter(List<Product> list_data, Context ct) {
        this.list_data = list_data;
        this.ct = ct;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.product_layout,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Product listData=list_data.get(position);
        Picasso.get()
                .load(listData.getPimage())
                .placeholder(R.drawable.defaultimg)
                .into(holder.img);
        Log.d("image",listData.getPimage());
        //create click listener
        holder.txt.setText(listData.getPname());
        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StaticInfo.selProdId=position;
                Log.d("sdfghsdfghjj",""+position);
                Intent intent=new Intent(ct, ProductDetails.class);
                intent.putExtra("catId",String.valueOf(listData.getId()));
                intent.putExtra("catName",String.valueOf(listData.getPname()));
                intent.putExtra("catImg",String.valueOf(listData.getPimage()));

                ct.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView img;
        private TextView txt;

        public ViewHolder(View itemView) {
            super(itemView);
            img=(ImageView)itemView.findViewById(R.id.product_image);
            txt=(TextView)itemView.findViewById(R.id.product_title);
        }
    }
}