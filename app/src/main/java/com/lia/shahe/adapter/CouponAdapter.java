package com.lia.shahe.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.lia.shahe.view.CouponActivity;
import com.lia.shahe.R;
import com.lia.shahe.bo.Coupons;
import com.lia.shahe.view.OrderPreview;


import java.util.List;

public class CouponAdapter extends RecyclerView.Adapter<CouponAdapter.ViewHolder> {
    List<Coupons> list_data;
    Context ct;
    private CouponActivity couponActivity;
    public CouponAdapter(List<Coupons> list_data, Context ct,CouponActivity couponActivity) {
        this.list_data = list_data;
        this.ct = ct;
        this.couponActivity = couponActivity;
    }

    @Override
    public CouponAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_coupon_layout, parent, false);
        return new CouponAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CouponAdapter.ViewHolder holder, final int position) {
        final Coupons listData = list_data.get(position);
        holder.coupon_codetxt.setText(listData.getcCode());
        holder.off_desctxt.setText(listData.getcDesc());
        Double dAmt=0.0;
        if (listData.getCtype().equals("SAR")){
            dAmt =listData.getcAmt();
        }else {
            Double tot=couponActivity.cOrderTot;
            double res = (tot / 100.0f) * listData.getcAmt();
            dAmt=res;
        }
        final Double finalDAmt = dAmt;
        holder.apply_coupontxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ct, OrderPreview.class);
                intent.putExtra("addressId",couponActivity.addressId);
                intent.putExtra("couponId",String.valueOf(listData.getId()));
                intent.putExtra("sCorderTot",finalDAmt);
                ct.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView coupon_codetxt,off_desctxt,apply_coupontxt;

        public ViewHolder(View itemView) {
            super(itemView);
            coupon_codetxt = (TextView) itemView.findViewById(R.id.coupon_code);
            off_desctxt = (TextView) itemView.findViewById(R.id.off_desc);
            apply_coupontxt = (TextView) itemView.findViewById(R.id.apply_coupon);
        }
    }

}
