package com.lia.shahe.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.lia.shahe.view.OrderDetails;
import com.lia.shahe.view.OrderListActivity;
import com.lia.shahe.R;
import com.lia.shahe.bo.Order;
import com.lia.shahe.utility.Session;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.ViewHolder> {
    List<Order> list_order;

    Context ct;
    private Session session;
    private OrderListActivity orderListActivity;

    private long lastClickTime = 0;



    public OrderListAdapter(List<Order> list_order, Context ct,Session session,OrderListActivity orderListActivity) {
        this.list_order = list_order;
        this.ct = ct;
        this.session=session;
        this.orderListActivity=orderListActivity;
    }

    @Override
    public OrderListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.order_layout,parent,false);
        return new OrderListAdapter.ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(final OrderListAdapter.ViewHolder holder, final int position) {
        final Order listOrderData=list_order.get(position);
        list_order.size();
    Log.d("sdfgh",String.valueOf(""+list_order.size()+"  "+list_order.get(position)));
        try {
            Log.d("asdfvb",listOrderData.getOrderDate()+"ostatus"+listOrderData.getOrderStatus()+"phone"+
                    listOrderData.getApporderId());
            holder.odatetxt.setText(listOrderData.getOrderDate());
            holder.ostatustxt.setText(listOrderData.getOrderStatus());
            holder.ordertxt.setText("#sho"+listOrderData.getApporderId());
            holder.opricetext.setText(""+listOrderData.getOrderTotal()+"SAR");
            holder.vattext.setText(""+listOrderData.getVat()+"%");
            Log.d("orddetails",""+listOrderData.getAddressDetails());

            double res = (listOrderData.getOrderTotal() / 100.0f) * Double.parseDouble(String.valueOf(listOrderData.getVat()));
            Double ototPrice=listOrderData.getOrderTotal()+res;
            holder.tottext.setText(""+ototPrice+"SAR");
            JSONObject orderjson=new JSONObject(listOrderData.getAddressDetails());
            holder.addrresstxt.setText(orderjson.getString("address"));
            holder.mobileltext.setText(orderjson.getString("phone"));
            holder.blinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("asdc",""+listOrderData.getDetails());
                    Intent intent=new Intent(ct, OrderDetails.class);
                    intent.putExtra("details",listOrderData.getDetails());
                    ct.startActivity(intent);
                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }



    }

    @Override
    public int getItemCount() {
        return list_order.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView img,pimg;
        private TextView txt,odatetxt,ostatustxt,ordertxt,addrresstxt,mobileltext,opricetext,vattext,tottext;
        private LinearLayout blinear;

        public ViewHolder(View itemView) {
            super(itemView);

            odatetxt=(TextView)itemView.findViewById(R.id.odate);
            ostatustxt=(TextView)itemView.findViewById(R.id.ostatus);
            ordertxt=(TextView)itemView.findViewById(R.id.ordtxt);
            addrresstxt=(TextView)itemView.findViewById(R.id.addrtxt);
            mobileltext=(TextView)itemView.findViewById(R.id.mobileltxt);
            opricetext=(TextView)itemView.findViewById(R.id.oprice);
            vattext=(TextView)itemView.findViewById(R.id.vattxt);
            tottext=(TextView)itemView.findViewById(R.id.totaltxt);

            blinear=(LinearLayout)itemView.findViewById(R.id.viewlinear);
        }
    }
}