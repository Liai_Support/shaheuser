package com.lia.shahe.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.shahe.R;
import com.lia.shahe.bo.Cart;
import com.lia.shahe.utility.Session;
import com.lia.shahe.utility.StaticInfo;
import com.lia.shahe.view.CartActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.List;

import static com.lia.shahe.view.CartActivity.cartPrice;
import static java.lang.Float.parseFloat;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {
    List<Cart> list_cart;

    Context ct;
    private Session session;
    private CartActivity cartActivity;
    private String Url="updatecart";
    private long lastClickTime = 0;

//    private String totCartprice=0;

    public CartAdapter(List<Cart> list_cart, Context ct,Session session,CartActivity cartActivity) {
        this.list_cart = list_cart;
        this.ct = ct;
        this.session=session;
        this.cartActivity=cartActivity;
    }

    @Override
    public CartAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_layout,parent,false);
        return new CartAdapter.ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(final CartAdapter.ViewHolder holder, int position) {
        final Cart listCartData=list_cart.get(position);
        final  TextView totCartprice=(TextView)((Activity)cartActivity).findViewById(R.id.total);
//        totCartprice.setText("0");

        try {
            JSONArray cartArray=new JSONArray(listCartData.getDetails());

            Double caprice= Double.valueOf(0);

            for (int i = 0; i < cartArray.length(); i++) {
                final JSONObject cartobj = cartArray.getJSONObject(i);
                cartobj.getString("userId");
                Log.d("sdfgfghj",""+cartobj.getString("userId"));
                holder.productname.setText(""+cartobj.getString("productname"));
                Picasso.get()
                        .load(cartobj.getString("pimage"))
                        .into(holder.pimg);
                holder.qtytext.setText(""+cartobj.getString("qty"));
                holder.qtytxt.setText(""+cartobj.getString("qty"));
                holder.branchtxt.setText(""+cartobj.getString("branchName"));
                final JSONObject pdetailsobj=new JSONObject(cartobj.getString("productDetails"));
                holder.size.setText(pdetailsobj.getString("desc")+"("+pdetailsobj.getString("price")+")");
                JSONObject cstyledetailsobj=new JSONObject(cartobj.getString("cuttingStyle"));
                holder.cstyletxt.setText(cstyledetailsobj.getString("desc"));
                JSONObject pstyledetailsobj=new JSONObject(cartobj.getString("packingStyle"));
                holder.pstyletxt.setText(pstyledetailsobj.getString("desc"));
                final JSONObject kheemadetailsobj=new JSONObject(cartobj.getString("kheemaDetails"));
                holder.Kheematxt.setText(kheemadetailsobj.getString("desc")+"("+kheemadetailsobj.getString("price")+")");
              final Double cprice=Double.parseDouble(cartobj.getString("qty"))*Double.parseDouble(pdetailsobj.getString("price"));
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    double totprice=Double.sum(cprice, parseFloat(""+kheemadetailsobj.getString("price")));
                    holder.cpricetxt.setText(""+totprice);
                    caprice=caprice+totprice;
                    Log.d("asd",""+caprice);
                }else {
                    double totprice=cprice+Float.parseFloat(""+kheemadetailsobj.getString("price"));
                    holder.cpricetxt.setText(""+totprice);
                    caprice=caprice+totprice;
                    Log.d("asd"+cartArray.getJSONObject(i),""+caprice);
                }



                holder.minusbtntxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int qty = Integer.parseInt(String.valueOf(holder.qtytxt.getText()))+1;

//                        Double tprice= null;
                        try {
                           Double  cprice = qty*Double.valueOf(pdetailsobj.getString("price"));
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                 double totprice=Double.sum(cprice, parseFloat(""+kheemadetailsobj.getString("price")));
                                holder.cpricetxt.setText(""+totprice);
                               cartPrice= cartPrice+Double.parseDouble(pdetailsobj.getString("price"));
                                totCartprice.setText(""+ cartPrice);
                            }else {
                                double totprice=cprice+Float.parseFloat(""+kheemadetailsobj.getString("price"));
                                holder.cpricetxt.setText(""+totprice);
                                cartPrice= cartPrice+Double.parseDouble(pdetailsobj.getString("price"));
                                totCartprice.setText(""+ cartPrice);
                            }

                           holder.qtytext.setText(""+qty);
                            holder.qtytxt.setText(""+qty);
                            JSONObject obj=new JSONObject();
                            try {
                                obj.put("cartid",listCartData.getId() );
                                obj.put("userid", session.getUserId());
                                obj.put("qty", qty);
                                updatecartJson(String.valueOf(obj));
                                Log.d("updatecartJsonrequest",""+obj);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
//                            updatecartJson();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
                holder.plusbtntxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        int qty = Integer.parseInt(String.valueOf(holder.qtytxt.getText()))-1;

                        if (qty>=1) {


                            Double  cprice = null;
                            try {
                                cprice = qty*Double.valueOf(pdetailsobj.getString("price"));
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    double totprice=Double.sum(cprice, parseFloat(""+kheemadetailsobj.getString("price")));
                                    holder.cpricetxt.setText(""+totprice);
                                    cartPrice= cartPrice-Double.parseDouble(pdetailsobj.getString("price"));
                                    totCartprice.setText(""+ cartPrice);
                                }else {
                                    double totprice=cprice+Float.parseFloat(""+kheemadetailsobj.getString("price"));

                                    holder.cpricetxt.setText(""+totprice);
                                    cartPrice= cartPrice-Double.parseDouble(pdetailsobj.getString("price"));
                                    totCartprice.setText(""+ cartPrice);
                                }
                                holder.qtytext.setText(""+qty);
                                holder.qtytxt.setText(""+qty);
                                JSONObject obj=new JSONObject();
                                try {
                                    obj.put("cartid",listCartData.getId() );
                                    obj.put("userid", session.getUserId());
                                    obj.put("qty", qty);
                                    updatecartJson(String.valueOf(obj));
                                    Log.d("updatecartJsonrequest",""+obj);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }
                });
                holder.removetxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // preventing double, using threshold of 1000 ms
                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000){
                            return;
                        }

                        lastClickTime = SystemClock.elapsedRealtime();
                        cartPrice=0.0;
                        JSONObject obj=new JSONObject();
                        try {
                            obj.put("cartid",listCartData.getId() );
                            obj.put("userid", session.getUserId());
                            cartActivity.updatecartdelJson(String.valueOf(obj));
                            Log.d("updatecartJsonrequest",""+obj);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });

              /*  holder.blinear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // preventing double, using threshold of 1000 ms
                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000){
                            return;
                        }

                        lastClickTime = SystemClock.elapsedRealtime();

                        Intent intent=new Intent(ct, UpdateCartActivity.class);
                        intent.putExtra("cartId",""+listCartData.getId());
                        intent.putExtra("details",""+listCartData.getDetails());
                        ct.startActivity(intent);


                    }
                });*/

            }
            cartPrice= cartPrice+caprice;
           caprice=caprice;

            Log.d("asd outer1",""+caprice);
            totCartprice.setText(""+ cartPrice);
            Log.d("sdfghj",""+cartArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }



    }

    private void updatecartJson(String response) {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(ct);
            JSONObject jsonBody = new JSONObject(response);

            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        Log.d("json response",""+response);

                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error").equals(true)){
                            Toast.makeText(cartActivity,"Oops Error while fetch data",Toast.LENGTH_SHORT).show();
                        }else{

//                            JSONArray dataobj=obj.getJSONArray("data");

                            JSONObject cartobj= obj.getJSONObject("data");
                            StaticInfo.cartCount= Integer.parseInt(cartobj.getString("cartCount"));



                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }


            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return list_cart.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView img,pimg;
        private TextView txt,productname,size,cstyletxt,qtytxt,Kheematxt,pstyletxt,branchtxt,cpricetxt,plusbtntxt,minusbtntxt,removetxt,qtytext;

        private LinearLayout blinear;

        public ViewHolder(View itemView) {
            super(itemView);
//            img=(ImageView)itemView.findViewById(R.id.branch_img);
            pimg=(ImageView)itemView.findViewById(R.id.pimg);
//            txt=(TextView)itemView.findViewById(R.id.branch);
            productname=(TextView)itemView.findViewById(R.id.prod_name);
            size=(TextView)itemView.findViewById(R.id.psize);
            cstyletxt=(TextView)itemView.findViewById(R.id.cstyle);
            qtytxt=(TextView)itemView.findViewById(R.id.qty);
            qtytext=(TextView)itemView.findViewById(R.id.qtytext);
            Kheematxt=(TextView)itemView.findViewById(R.id.Kheema);
            pstyletxt=(TextView)itemView.findViewById(R.id.pstyle);
            cpricetxt=(TextView)itemView.findViewById(R.id.cprice);
            branchtxt=(TextView)itemView.findViewById(R.id.branch);
            plusbtntxt=(TextView)itemView.findViewById(R.id.plusbtn);
            minusbtntxt=(TextView)itemView.findViewById(R.id.minusbtn);
            removetxt=(TextView)itemView.findViewById(R.id.remove);
            blinear=(LinearLayout)itemView.findViewById(R.id.cartlinear);
        }
    }
}