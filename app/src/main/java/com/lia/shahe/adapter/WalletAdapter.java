package com.lia.shahe.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.lia.shahe.R;
import com.lia.shahe.bo.Wallet;

import java.util.List;

public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.ViewHolder> {
    List<Wallet> list_data;
    Context ct;

    public WalletAdapter(List<Wallet> list_data, Context ct) {
        this.list_data = list_data;
        this.ct = ct;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.wallet_history_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Wallet listData = list_data.get(position);

        //create click listener
        holder.entrydesctxt.setText(listData.getWdesc());
        holder.entryamttxt.setText("+"+listData.getWamount()+" SAR");
        holder.entrydatetxt.setText(listData.getWdate());
        if (listData.getWtype().equals("credit")){
            holder.entryamttxt.setTextColor(ContextCompat.getColor(ct,R.color.appColor));
        }else{
            holder.entryamttxt.setTextColor(ContextCompat.getColor(ct,R.color.redColor));
        }



    }

    @Override
    public int getItemCount() {
        return list_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView entrydesctxt,entryamttxt,entrydatetxt;

        public ViewHolder(View itemView) {
            super(itemView);
            entrydesctxt = (TextView) itemView.findViewById(R.id.entrydesc);
            entryamttxt = (TextView) itemView.findViewById(R.id.entryamt);
            entrydatetxt = (TextView) itemView.findViewById(R.id.entrydate);
        }
    }
}