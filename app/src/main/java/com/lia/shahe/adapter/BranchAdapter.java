package com.lia.shahe.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.lia.shahe.view.HomeScreen;
import com.lia.shahe.R;
import com.lia.shahe.bo.Branch;
import com.lia.shahe.utility.Session;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BranchAdapter extends RecyclerView.Adapter<BranchAdapter.ViewHolder> {
    List<Branch> list_branch;
    Context ct;
    private Session session;

    public BranchAdapter(List<Branch> list_branch, Context ct,Session session) {
        this.list_branch = list_branch;
        this.ct = ct;
        this.session=session;
    }

    @Override
    public BranchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.branch_layout,parent,false);
        return new ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(BranchAdapter.ViewHolder holder, int position) {
        final Branch listBranchData=list_branch.get(position);

        Picasso.get()
                .load(listBranchData.getbImage())
                .placeholder(R.drawable.defaultimg)
                .into(holder.img);

        Log.d("sdfgh",""+listBranchData.getbImage());
        //create click listener
        holder.txt.setText(listBranchData.getAddress());
        holder.blinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                session.setBranchId(listBranchData.getId());
                session.setBranchAddr(listBranchData.getAddress());
                Intent intent=new Intent(ct, HomeScreen.class);
                ct.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list_branch.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView img;
        private TextView txt;
        private LinearLayout blinear;

        public ViewHolder(View itemView) {
            super(itemView);
            img=(ImageView)itemView.findViewById(R.id.branch_img);
            txt=(TextView)itemView.findViewById(R.id.branch_txt);
            blinear=(LinearLayout)itemView.findViewById(R.id.branch_linear);
        }
    }
}